@extends("admin.layouts.default")
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="title-button">
            <h1 class="page-heading">Category</h1>
            <a class="btn btn-primary btn-additem" href="{{route('category.create')}}">Add <i class="fa fa-plus"></i></a>
        </div>
        <div class="the-box">
            <div class="table-responsive">
            <table class="table table-striped table-hover" id="datatable-example">
                <thead class="the-box dark full">
                    <tr>
                        <th> Image</th>
                        <th>Title</th>
                        <th>Order By</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)

                    <tr class="odd gradeX">
                         <td>
                            <img src="{{ url('/uploads/category').'/'.$category->image}}" width="50" height="50">
                        </td>
                        <td>{{$category->title}}</td>
                         <td>{{$category->order_by}}</td>
                        <td class="center">
                            <a href="{{route('category.edit',['id' => $category->id])}}" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                            <form action="{{route('category.destroy',['id' => $category->id])}}" method="post" style="display: inline-block;">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash-o"></i></button>
                            </form>
                        </td>
                    </tr>
                    
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection
