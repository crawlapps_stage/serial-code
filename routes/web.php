<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth.shop']], function () {
Route::get('/', function () {
       $obSerialcode = new \App\SerialCode();
       $obSerialcode->checkscriptTag();
      return redirect('/serialcode');
    })->name('home');
//Route::get('/', 'SerialCodeController@index')->middleware(['auth.shop'])->name('home');
//Route::get('/', 'SerialCodeController@index');
 
Route::get('flush', function() {
    request()->session()->flush();
});
Route::get('/serialcode', 'SerialCodeController@index')->name('serialcode.index');
Route::post('/serialcode', 'SerialCodeController@store')->name('serialcode.store');
Route::get('/serialcode/create', 'SerialCodeController@create')->name('serialcode.create');
Route::get('/serialcode/{serialcode}/edit', 'SerialCodeController@edit')->name('serialcode.edit');
Route::patch('/serialcode/{serialcode}', 'SerialCodeController@update')->name('serialcode.update');

Route::post('/serialcode/{serialcode}', 'SerialCodeController@update')->name('serialcode.update');
Route::get('/serialcode/search', 'SerialCodeController@search')->name('serialcode.search');
Route::delete('serialcode-delete-multiple', ['as'=>'serialcode.multiple-delete','uses'=>'SerialCodeController@deleteMultiple']);
Route::delete('serialcode-delete-single', ['as'=>'serialcode.single-delete','uses'=>'SerialCodeController@Singledelete']);
Route::get('customer-search-group', ['as'=>'customers.groups','uses'=>'CustomerController@getCustomersearchGroup']);
Route::get('customer-search', ['as'=>'customers.specific','uses'=>'CustomerController@getCustomer']);


Route::get('serialcode-status-multiple', ['as'=>'serialcode.multiple-status','uses'=>'SerialCodeController@activecode']);
Route::get('customergroup', ['as'=>'customergroup.search','uses'=>'CustomerController@search']);
Route::get('export', 'SerialCodeController@export')->name('serialcode.export');
});
Route::get('settings', 'SettingController@index')->name('settings.index');
Route::get('settings/create', 'SettingController@create')->name('settings.create');
Route::get('settings/filter', 'SettingController@filter')->name('settings.filter');
Route::get('settings/{id}/edit', 'SettingController@edit')->name('settings.edit');
Route::post('settings', 'SettingController@store')->name('settings.store');
Route::post('settings/{id}', 'SettingController@update')->name('settings.update');
Route::get('customers', 'CustomerController@index')->name('customers.index');
Route::get('customers/filter', 'CustomerController@filter')->name('customers.filter');
Route::delete('customers/delete', 'CustomerController@destroy')->name('customers.delete');

Route::get('check-webhook', function(\Request $request){
    $shop = \ShopifyApp::shop();
    $data = $shop->api()->rest("GET", '/admin/webhooks.json');
    echo config("shopify-app.api_scopes"); exit;  
});

Route::get('emailtemplate/create', 'EmailTemplateController@create')->name('emailtemplate.create');
Route::post('emailtemplate', 'EmailTemplateController@store')->name('emailtemplate.store');

Route::group(['middleware' => 'cors'], function() {
   Route::get('customerform/{demail}', 'CustomerController@getcustomerform')->name('customers.getform');
   Route::any('serialcodefront/checkserialcode', 'SerialCodeController@checkserialcode')->name('serialcodefront.checkserialcode');
 Route::any('serialcodefront/applycode', 'SerialCodeController@applycode')->name('serialcodefront.applycode');
   

});

