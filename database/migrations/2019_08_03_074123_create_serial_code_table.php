<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerialCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serial_code', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shop_id');
            $table->string('serial_code');
            $table->enum('customer_eligibility', ['1', '2','3']);
            $table->enum('limit_number_of_time', ['0', '1'])->default('0');
            $table->string('limit_number_user')->nullable();
            $table->enum('limit_one_per_user', ['0', '1'])->default('0');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->time('start_time')->nullable();
            $table->integer('is_register')->default('0');
            $table->enum('is_end_date', ['0', '1'])->default('0');
            $table->enum('status', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serial_code');
    }
}
