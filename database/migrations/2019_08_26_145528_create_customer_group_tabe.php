<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerGroupTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_group_id');
            $table->unsignedBigInteger('serial_code_id');
            $table->string('group_title');
            $table->timestamps();
        });
        Schema::table('customer_group', function($table) {
            $table->foreign('serial_code_id')->references('id')->on('serial_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_group_tabe');
    }
}
