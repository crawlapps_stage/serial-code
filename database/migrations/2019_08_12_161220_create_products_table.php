<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_id');
            $table->unsignedBigInteger('serial_code_id');
            $table->string('product_title',1000);
            $table->string('image',1000)->nullable();
            $table->timestamps();
        });

        Schema::table('products', function($table) {
            $table->foreign('serial_code_id')->references('id')->on('serial_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
