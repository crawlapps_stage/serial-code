<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseSerialCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('use_serial_codes', function (Blueprint $table) {
            $table->increments('id'); 
            $table->unsignedBigInteger('customer_id');
            $table->unsignedInteger('shop_id');
            $table->string('product_id');
            $table->string('serial_id');
            $table->timestamps();
        });

         Schema::table('use_serial_codes', function($table) {
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('shop_id')->references('id')->on('shops')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('use_serial_codes');
    }
}
