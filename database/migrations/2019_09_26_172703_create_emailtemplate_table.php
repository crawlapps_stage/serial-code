<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailtemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailtemplate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('subject');
            $table->longText('content');
            $table->unsignedInteger('shop_id');
            $table->timestamps();
        });

        Schema::table('emailtemplate', function($table) {
            $table->foreign('shop_id')->references('id')->on('shops');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emailtemplate');
    }
}
