<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificCustomerTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specific_customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('specific_customer_id');
            $table->unsignedBigInteger('serial_code_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->timestamps();
        });
        Schema::table('specific_customer', function($table) {
            $table->foreign('serial_code_id')->references('id')->on('serial_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specific_customer_tabe');
    }
}
