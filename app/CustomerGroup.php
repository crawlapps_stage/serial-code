<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    protected $table = 'customer_group';
    protected $fillable = [
        'customer_group_id', 'serial_code_id','product_title',
    ];

    public function serialcode()
    {
        return $this->belongsTo('App\SerialCode');
    }

    public function CustomerGroup($request,$id){

    	foreach ($request as $group) {
    	   $obCustomergroup  = (isset($group['id'])) ? CustomerGroup::find($group['id']) : new CustomerGroup;
    	     $obCustomergroup->customer_group_id =  $group['group_id']; 
           $obCustomergroup->group_title =  $group['group_name'];
           $obCustomergroup->serial_code_id =  $id;
           $obCustomergroup->save();
        }
    }

    
}
