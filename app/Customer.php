<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SerialCode;

class Customer extends Model
{
	 protected $table = 'customers';

    public function serialcode()
    {
        return $this->belongsTo('App\SerialCode');
    }

       public function userSeralcode()
       {
       	return $this->hasMany('App\UseSerialCode','customer_id','id');
        }
    

    public function customerfilter($request){

	    $shop = \ShopifyApp::shop(); 
	    $keyword = $request['search'];
	    $action = $request['action'];
	    $perpage = $request['perpage'];
	    $lastItem = ($action == 'search') ? 0 : $request['lastItem'];

	     $customersquery = Customer::with(['userSeralcode'])->where(function($query1) use ($keyword) {
	            $query1->where('first_name', 'like', '%' . $keyword . '%');
	            $query1->where('last_name', 'like', '%' . $keyword . '%');
	            $query1->orwhere('email', 'like', '%' . $keyword . '%');
	            $query1->orwhere('created_at', 'like', '%' . $keyword . '%');
	        });
	        $getcustomers = $customersquery->where('shop_id','=',$shop->id)->orderBy('id', 'asc')->skip($lastItem)->take($perpage)->get();

  $customers = array();
  $counter = 0;
  foreach ($getcustomers as  $value) {

    $customers[$counter]['first_name'] = $value->first_name;
    $customers[$counter]['last_name'] = $value->last_name;
    $customers[$counter]['email'] = $value->email;
    $customers[$counter]['created_at'] = $value->created_at;
    $serialcodeval = '';
    $serialcodecount = 0;
    if(count($value->userSeralcode) > 0){
      foreach ($value->userSeralcode as  $serialvalue) {
             $serialcode = SerialCode::find($serialvalue->serial_id);
            if(!empty($serialcode)){
                if($serialcodecount == 0){
                 $serialcodeval = $serialcode->serial_code;
             }else{
              $serialcodeval = $serialcodeval.'<br>'.$serialcode->serial_code;
             }
             $serialcodecount++;
            }
      }
    }
    $customers[$counter]['serialcode'] = $serialcodeval;
    $counter++;
  }

	   if ($action == 'search') {
	            $lastItem = 0;
	            $count = $customersquery->count();
	            $totalpages = ceil($count / $perpage);
	            $response = [
	                'customers' => $customers,
	                'count' => $count,
	                'totalpages' => $totalpages
	            ];
	            return json_encode($response);
	        } else {
	            return json_encode($customers);
	        } 
    }
}
