<?php

namespace App;

use Illuminate\Console\Command;
use App\Customer;
use App\SpecificCustomer;

class TraitCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }

    public function Getcustomergroup($request){

        if($request['action'] == 'search' && $request['filter'] != ''){
          $customerRead =   $this->getShop()->api()->graph('{
                                          customerSavedSearches(query: "name:'.$request['filter'].'", first: 10) {
                                            edges {
                                              node {
                                                id
                                                name
                                              }
                                              cursor
                                            }
                                          }
                                        }');            
        }else{
           $customerRead =   $this->getShop()->api()->graph('{
                      customerSavedSearches(first: 250, query: "") {
                        edges {
                          cursor
                          node {
                            id
                            name
                            query
                            searchTerms
                            resourceType
                          }
                        }
                      }
                    }');              
        }

          $GroupList =  $customerRead->body;
          $CustomerListgroup = [];
          $counter = 0;
          if(!empty($GroupList->customerSavedSearches->edges)){
             foreach ($GroupList->customerSavedSearches->edges as $GroupList) {                 
                        $CustomerListgroup[$counter]['cursor'] = $GroupList->cursor;
                        $CustomerListgroup[$counter]['group_id'] = $GroupList->node->id;
                        $CustomerListgroup[$counter]['group_name'] = $GroupList->node->name;                
                         $counter++;                     
             }
         }
         return $CustomerListgroup;
    }
 
 /********************Get store detail ***********************/
    public function getShop(){
        return \ShopifyApp::shop();
    }

    public function Getcustomer($request){
      if($request['action'] == 'search' && $request['filter'] != ''){
        $customerRead =   $this->getShop()->api()->graph('{
                                                        customers(first: 10, query: "firstName:'.$request['filter'].',lastName:'.$request['filter'].'") {
                                                          edges {
                                                            node {
                                                              id
                                                              firstName
                                                              lastName
                                                              email
                                                            }
                                                            cursor
                                                          }
                                                        }
                                                      }');     

      }else{
        $customerRead =   $this->getShop()->api()->graph('{
                                              customers(first: 25) {
                                                edges {
                                                  node {
                                                    id
                                                    firstName
                                                    lastName
                                                    email
                                                  }
                                                  cursor
                                                }
                                              }
                                            }');
       }
        $CustomerList = $customerRead->body;
        $Customerarray = [];
          $counter = 0;
          if(!empty($CustomerList->customers->edges)){
             foreach ($CustomerList->customers->edges as $CustomerList) {
                        $Customerarray[$counter]['cursor'] = $CustomerList->cursor;
                        $Customerarray[$counter]['customer_id'] = $CustomerList->node->id;
                        $Customerarray[$counter]['firstname'] = $CustomerList->node->firstName; 
                        $Customerarray[$counter]['lastname'] = $CustomerList->node->lastName;
                        $Customerarray[$counter]['email'] = $CustomerList->node->email;                 
                         $counter++;                     
             }
         }
         return $Customerarray;        
       }

     public function customersearch($shop,$email){
       $customerRead =   $shop->api()->graph('{
            customers(query: "email:'.$email.'", first: 25) {
              edges {
                cursor
                node {
                  id
                  email
                  firstName
                  lastName
                  tags
                }
              }
            }
          }');

     $CustomerList = $customerRead->body;
         $customerRersponse = '';
         if(!empty($CustomerList->customers->edges[0])){
            $customerRersponse = $CustomerList->customers->edges[0]->node;
         } 

        return $customerRersponse;     
    }

    public function customertagpdate($customerId,$shop,$productid){

       $getCustomerDetail = $shop->api()->rest('GET', '/admin/customers/'.$customerId.'.json');
       $customer = $getCustomerDetail->body;
       $productname = $this->GetProductName($productid);
       $CustomerTag[0] = 'serialcode-register'; 
       $CustomerTag[1] = 'serialcode-'.$productname;
       $tags = '';
        if(!empty($customer->customer->tags)){
           $oldTags = $customer->customer->tags;
           $tagArray =  explode(',',$customer->customer->tags);
           $trimArray = array_filter(array_map('trim', $tagArray));
                
           foreach ($CustomerTag as $value) {
             if (!in_array($value, $trimArray))
                  {
                     if(empty($tags)){
                         $tags = $value;
                     }else{
                         $tags = $tags.','.$value;
                     }                 
                  }
           }         
        }

      if(empty($tags)){

        if(empty($oldTags)){
           $tagadd = 'serialcode-register,serialcode-'.$productname;
        }else{
          $tagadd = $oldTags.',serialcode-register,serialcode-'.$productname;
        }
              $CustomerupdateRequest = $shop->api()->rest('PUT', '/admin/customers/'.$customerId.'.json',
       ['customer' => ['tags' => $tagadd ]]);       
      }else{
           $CustomerupdateRequest = $shop->api()->rest('PUT', '/admin/customers/'.$customerId.'.json',
       ['customer' => ['tags' => $oldTags.','.$tags ]]);       
      }
     $Response = $CustomerupdateRequest->body;   
    }

   public function customercreate($shop,$customerdetail){
     $productname = $this->GetProductName($customerdetail['product_id']);
     $customerCreate =   $shop->api()->graph('  mutation {
           customerCreate(input: { email:"'.$customerdetail['user_mail'].'", firstName: "'.$customerdetail['firstname'].'", lastName: "'.$customerdetail['lastname'].'", tags: "serialcode-register,serialcode-'.$productname.'" }) {
              customer {
                id
                firstName
                lastName
                email
              }
            }
          }');
       $customer = '';
       $CreateCustomerResponse = $customerCreate->body;
       if(!empty($CreateCustomerResponse->customerCreate->customer)){
          $customer = $CreateCustomerResponse->customerCreate->customer;
       }
        return $customer;
   } 

   public function GetProductName($productId){
      $Productdetail = Product::where('product_id','=',$productId)->first();
      $product = '';
      if(!empty($Productdetail)){
          $product = $Productdetail->product_title;
      }
      return $product;
   }

   public function customerGroupcheck($shop,$Groupid,$customerId){
    
    $getgroup = $shop->api()->rest('GET', '/admin/customer_saved_searches/'.$Groupid.'/customers.json');

     $shopifycustomerId = $this->getShopifyCustomerId($customerId);    
     $GroupList = $getgroup->body;
      $isvarify = '';
     if(!empty($GroupList->customers)){
        foreach ($GroupList->customers as $groupvalue) {          
          if($groupvalue->id == $shopifycustomerId){
               return $groupvalue->id;
          }         
        }
     }
     return $isvarify; 
 }

   public function specificcustomercheck($customerId){
    $customerDetail = '';
    $shopifycustomerId = $this->getShopifyCustomerId($customerId);

    if(!empty($shopifycustomerId)){
         $specificCustomerdetail = SpecificCustomer::where('specific_customer_id','=',$shopifycustomerId)->first();
         if(!empty($specificCustomerdetail)){               
            $customerDetail = $shopifycustomerId;
         }
    }
      return $customerDetail;
   }

   public function getShopifyCustomerId($customerId){
     $Customerdetail = Customer::where('id','=',$customerId)->first();
     $customerApp = '';
     if(!empty($Customerdetail->shopify_customer_id)){
         $customerApp =  $Customerdetail->shopify_customer_id;
     }
     return $customerApp;
   }
}
     