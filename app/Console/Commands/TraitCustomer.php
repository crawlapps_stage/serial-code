<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TraitCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }

    public function Getcustomergroup(){
        
              $customerRead =   $this->getShop()->api()->graph('{
                  customerSavedSearches(first: 250, query: "") {
                    edges {
                      cursor
                      node {
                        id
                        name
                        query
                        searchTerms
                        resourceType
                      }
                    }
                  }
                }');

          $GroupList =  $customerRead->body;
          $CustomerListgroup = [];
          $counter = 0;
          if(!empty($GroupList->customerSavedSearches->edges)){
             foreach ($GroupList->customerSavedSearches->edges as $GroupList) {                 
                        $CustomerListgroup[$counter]['cursor'] = $GroupList->cursor;
                        $CustomerListgroup[$counter]['group_id'] = $GroupList->node->id;
                        $CustomerListgroup[$counter]['group_name'] = $GroupList->node->name;                
                         $counter++;                     
             }
         }
         return $CustomerListgroup;
    }


    public function Searchcustomergroup($request){
   
      $customerRead =   $this->getShop()->api()->graph('{
    customers(name: "new", first: 1) {
      edges {
        cursor
        node {
            id
            name
            query
            searchTerms
            resourceType
          }
      }
    }
  }');
$GroupList =  $customerRead->body;
        echo '<pre>'; print_r($GroupList); exit;

    }

    public function getShop(){
        return \ShopifyApp::shop();

    }

    public function Getcustomer(){
        $customerRead =   $this->getShop()->api()->graph('{
                                              customers(first: 10) {
                                                edges {
                                                  node {
                                                    id
                                                    firstName
                                                    lastName
                                                  }
                                                  cursor
                                                }
                                              }
                                            }');

            $CustomerList = $customerRead->body;
        $Customerarray = [];
          $counter = 0;
          if(!empty($CustomerList->customers->edges)){
             foreach ($CustomerList->customers->edges as $CustomerList) {
                        $Customerarray[$counter]['cursor'] = $CustomerList->cursor;
                        $Customerarray[$counter]['customer_id'] = $CustomerList->node->id;
                        $Customerarray[$counter]['firstname'] = $CustomerList->node->firstName; 
                         $Customerarray[$counter]['lastname'] = $CustomerList->node->lastName;                
                         $counter++;                     
             }
         }

         return $Customerarray;        
       }
}
