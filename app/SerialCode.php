<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use DB;
use Response;

class SerialCode extends Model
{
	protected $table = 'serial_code';
    
    protected $fillable = [
        'shop_id','serial_code', 'product_id', 'customer_eligibility','user_limit','start_date','end_date','start_time','status','customer_group'
    ];
    protected $casts = [
        'product_id' => 'array', 'customer_group' => 'array','specific_customer' => 'array' ];

    public function collection()
    {
        return SerialCode::all();
    }

  public function checkscriptTag(){
        $shop = \ShopifyApp::shop();
        $Checkscripttag = $shop->api()->rest('GET', '/admin/script_tags.json');
        $ScriptTagList = $Checkscripttag->body;
        $flag = 0;
         if(!empty($ScriptTagList->script_tags)){
             foreach ($ScriptTagList->script_tags as $scriptTagval){
                 if($scriptTagval->src == url('/js/serialcode.js')){
                    $flag = 1;
                 }
             }
         }
         if($flag == 0){
             $parameter = [];
             $parameter['script_tag']['event'] = "onload";
             $parameter['script_tag']['src'] = url('/js/serialcode.js');
             $CallApi = $shop->api()->rest('POST', '/admin/script_tags.json', $parameter);     
         }
    }
    
    public function belongsToManyCustomers()
    {
         return $this->belongsToMany('App\Customer','use_serial_codes','customer_id','serial_id')
        ->withPivot('created_at')
        ->orderBy('pivot_created_at', 'desc');
    }


    public function product() {
        return $this->hasMany('App\Product');
    }
    public function CustomerGroup() {
        return $this->hasMany('App\CustomerGroup');
    }
    public function SpecificCustomer() {
        return $this->hasMany('App\SpecificCustomer');
    }
     public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function getSerialcode($shopId,$offset,$perpage) {

      $serialcodederail = DB::table('serial_code')->where('serial_code.shop_id','=',$shopId)
                          ->leftJoin('use_serial_codes', 'serial_code.id', '=', 'use_serial_codes.serial_id')
                          ->leftJoin('customers', 'use_serial_codes.customer_id', '=', 'customers.id')
                          ->groupBy('serial_code.id')
                          ->orderBy('use_serial_codes.created_at','DESC')
                          ->select('serial_code.*','use_serial_codes.serial_id','customers.first_name','customers.last_name','customers.email')
                          ->take($perpage)->skip($offset)->get();

      $serialcode = array();
    if(!empty($serialcodederail)){         
          $counter = 0;
            foreach ($serialcodederail as $value) {
                $serialcode[$counter]['id'] = $value->id;
                $serialcode[$counter]['status'] = $value->status;
                $serialcode[$counter]['serial_code'] = $value->serial_code;
                $serialcode[$counter]['start_date'] =  date('M d', strtotime($value->start_date));
                if(!empty($value->serial_id)){
                    $serialcode[$counter]['message'] = $value->first_name." ".$value->last_name.'<br>'.$value->email;
                }else{
                    $Usagelimit = ($value->limit_number_user != '') ? $value->limit_number_user : '∞';
                    $serialcode[$counter]['message'] ='0 registered of '.$Usagelimit.' total';
                }
                $counter++;
             }
         }     
       return $serialcode;
     }
    public function  Serialcodefilter($request){
 
        $shop = \ShopifyApp::shop(); 
    	$keyword = $request['search'];
        $action = $request['action'];
        $perpage = $request['perpage'];
        $lastItem = ($action == 'search') ? 0 : $request['lastItem'];
        $sorting = $request['sorting'] ? $request['sorting'] : "'created_at', 'desc'";      
        $sortexpload = explode(',', $sorting); 
        $Usage = ($request['registerstatus'] == '1') ? 1 : 0;
        $shopId = strval($shop->id);
        

         if (strpos($sortexpload[1], 'asc') !== false) {
                $orderType = 'asc';
            }else{
                $orderType = 'desc';
            } 

            $colunmName =  'serial_code.'.str_replace("'", "",$sortexpload[0]);      

        $serialcodederail = DB::table('serial_code')->where('serial_code.shop_id','=',$shopId)
            ->where('serial_code.serial_code', 'like', '%' . $keyword . '%');
     if($request['registerstatus'] == '1'){
        $serialcodederail = $serialcodederail->join('use_serial_codes', 'serial_code.id', '=', 'use_serial_codes.serial_id')
            ->groupBy('use_serial_codes.serial_id');         
     }else if($request['registerstatus'] == '2'){
        $serialcodederail = $serialcodederail->leftJoin('use_serial_codes', 'serial_code.id', '=', 'use_serial_codes.serial_id')
            ->whereNull('use_serial_codes.serial_id');                    
     } else{
        $serialcodederail = $serialcodederail->leftJoin('use_serial_codes', 'serial_code.id', '=', 'use_serial_codes.serial_id')
        ->groupBy('serial_code.id'); 
     }
    $serialcodederail = $serialcodederail->select('serial_code.*', 'use_serial_codes.serial_id')->orderBy($colunmName,$orderType)->take($perpage)->skip($lastItem)->get();
     $serialcode = array();
     $serialcodecounter = 0;
     if(!empty($serialcodederail)){
          $Getcustomer = '';
          $counter = 0;
            foreach ($serialcodederail as $value) {                
                    if(!empty($value->serial_id)){
                         $Getcustomer = DB::table('use_serial_codes')
                                    ->join('customers', 'use_serial_codes.customer_id', '=', 'customers.id')
                                    ->where('use_serial_codes.serial_id','=',$value->serial_id)
                                    ->select('customers.*','use_serial_codes.created_at');
                                if($request['registerstatus'] == '1'){
                                   $Getcustomer = $Getcustomer->orderBy('use_serial_codes.created_at','DESC')->groupBy('customers.id')->get();
                                }else{ 
                                  $Getcustomer = $Getcustomer->limit(1)->get();
                                }
                              
                            if(!empty($Getcustomer)){
                                foreach ($Getcustomer as $key => $customerval) {                                   
                                    $serialcode[$counter]['id'] = $value->id;
                                    $serialcode[$counter]['message'] = $customerval->first_name." ".$customerval->last_name.'<br>'.$customerval->email;                                  
                                    $serialcode[$counter]['serial_code'] = $value->serial_code;
                                    $serialcode[$counter]['start_date'] =  date('M d', strtotime($value->start_date));
                                    $serialcode[$counter]['status'] = $value->status;
                                    $serialcode[$counter]['customer_eligibility'] = $value->customer_eligibility;
                                    $serialcode[$counter]['limit_number_of_time'] = $value->limit_number_of_time;
                                    $serialcode[$counter]['limit_number_user'] = $value->limit_number_user;
                                     $serialcode[$counter]['status'] = $value->status;
                                    $counter++;
                               }
                            }
                    }else{
                        $serialcode[$counter]['id'] = $value->id;
                        $serialcode[$counter]['serial_code'] = $value->serial_code;
                        $Usagelimit = ($value->limit_number_user != '') ? $value->limit_number_user : '∞';
                        $serialcode[$counter]['message'] ='0 registered of '.$Usagelimit.' total';
                        $serialcode[$counter]['start_date'] =  date('M d', strtotime($value->start_date));
                        $serialcode[$counter]['status'] = $value->status;
                        $serialcode[$counter]['customer_eligibility'] = $value->customer_eligibility;
                        $serialcode[$counter]['limit_number_of_time'] = $value->limit_number_of_time;
                        $serialcode[$counter]['limit_number_user'] = $value->limit_number_user;
                        $serialcode[$counter]['status'] = $value->status;
                        $counter++;
                    }
                    
        }
       
     }

     if ($action == 'CSV') {
         $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
         );

            $reviews = $serialcode;
            $columns = array('Serial Code', 'Customer Eligibility', 'Start Date', 'Status');

            $callback = function() use ($reviews, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($reviews as $review) {
                    $status = ($review['status'] == '0') ? 'Active' : 'InActive';
                     $customerEligibility = $review['customer_eligibility'] == '1' ? 'Everyone' : ($review['customer_eligibility'] == '2' ? 'Specific Groups of Customers' : 'Specific Customers');
                    fputcsv($file, array($review['serial_code'], $customerEligibility,$review['start_date'], $status));
                }
                fclose($file);
            };
         return Response::stream($callback, 200, $headers);
     }

         if ($action == 'search') {
            $lastItem = 0;
            $count = count($serialcode);
            $totalpages = ceil($count / $perpage);
            $response = [
                'Serialcode' => $serialcode,
                'count' => $count,
                'totalpages' => $totalpages
            ];
            return json_encode($response);
        } else {
            return json_encode($serialcode);
        }
    }

    public function getcodeusers($id){
      return  $users = DB::table('use_serial_codes')
                     ->where('serial_id', '=', $id)
                     ->count();
    }
}
