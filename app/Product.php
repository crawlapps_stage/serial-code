<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'product_id', 'product_name','image',
    ];

     public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function serialcode()
    {
        return $this->belongsTo('App\SerialCode');
    }

    public function Productstore($request,$id){
      foreach ($request as $productval) {
    	   $obProduct  = (isset($productval['id'])) ? Product::find($productval['id']) : new Product;
    	     $obProduct->product_id =  $productval['product_id']; 
           $obProduct->product_title =  $productval['product_title'];
           $obProduct->image =  $productval['product_image'];
           $obProduct->serial_code_id =  $id;
           $obProduct->save();
       }
    }

}