<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;

class SerialcodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

         // $rules = [
         //    'product_id' => 'required|max:255',
         //    'customer_eligibility' => 'required|max:255',
         //    'start_date' => 'required',
         //    'products' => 'required',
         //  ];

          /*foreach($this->request->get('serial_code') as $key => $val)
          {
            $rules['serial_code.'.$key] = 'required|string|max:255';
          }
          dd($rules);
          return $rules;*/
        
          return $rules = [
            'serial_code.*' => 'required|string|max:255',
            ];
    }

    public function messages()
    {
        $messages = array();
       

        return $messages;
    }

    protected function failedValidation(Validator $validator)
   {
       if ($this->ajax() || $this->wantsJson()) {
           $response = new JsonResponse($validator->errors(), 422);
           throw new ValidationException($validator, $response);
       }
       throw (new ValidationException($validator))
           ->errorBag($this->errorBag)
           ->redirectTo($this->getRedirectUrl());
   }
}
