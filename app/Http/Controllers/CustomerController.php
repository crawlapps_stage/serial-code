<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TraitCustomer;
use App\Customer;
use App\SerialCode;
use OhMyBrew\ShopifyApp\Models\Shop;

class CustomerController extends Controller
{

	public function __construct()
    {
        $this->customrTraint  = new TraitCustomer();
    }

  public function index(){
    $offset = 0;
    $perpage = 20;
    $currentpage = 1;
    $shop = \ShopifyApp::shop(); 
    $count = Customer::count();
    $totalpages = ceil($count / $perpage);
    $getcustomers = Customer::with(['userSeralcode'])->where('shop_id','=',$shop->id)->skip($offset)->take($perpage)->get();
  
  $customers = array();
  $counter = 0;
  foreach ($getcustomers as  $value) {

    $customers[$counter]['id'] = $value->id;
    $customers[$counter]['first_name'] = $value->first_name;
    $customers[$counter]['last_name'] = $value->last_name;
    $customers[$counter]['email'] = $value->email;
    $customers[$counter]['created_at'] = $value->created_at;
    $serialcodeval = '';
    $serialcodecount = 0;
    if(count($value->userSeralcode) > 0){
      foreach ($value->userSeralcode as  $serialvalue) {
             $serialcode = SerialCode::find($serialvalue->serial_id);
            if(!empty($serialcode)){
                if($serialcodecount == 0){
                 $serialcodeval = $serialcode->serial_code;
             }else{
              $serialcodeval = $serialcodeval.','.$serialcode->serial_code;
             }
             $serialcodecount++;
            }
      }
    }

    $userserailcode = implode('<br>',array_unique(explode(',', $serialcodeval)));
    $customers[$counter]['serialcode'] = $userserailcode;
    $counter++;
  }
    $query = "";
      return view('customers.index',compact('customers', 'query', 'offset', 'perpage', 'currentpage', 'totalpages'));
  }

  public function filter(Request $request){
     $obSettings = new Customer();
     $customers =  $obSettings->customerfilter($request->all());
      return $customers; 
  }

    public function getCustomersearchGroup(Request $request){
     	/* Get customer Group */
          $customrTraint = new TraitCustomer();
          $CustomerListgroup = $this->customrTraint->Getcustomergroup($request->all());    	             
          return json_encode($CustomerListgroup);
    }

    public function search(Request $request){
         $shop = \ShopifyApp::shop();
         $CustomerListgroup = $this->customrTraint->Searchcustomergroup($request->all());  
         return json_encode($CustomerListgroup); 
     }

     public function getCustomer(Request $request){
          /* Get customer Group */
          $customrTraint = new TraitCustomer();
          $CustomerList = $this->customrTraint->Getcustomer($request->all());
                          
          return json_encode($CustomerList);   
     }

     public function getcustomerform($domain){
          $this->shop = Shop::where('shopify_domain', $domain)->first();
           $html =   view('customers.customerform');
           $html = (string) $html;
          return response()->json($html);           
     }

     public function destroy(Request $request){
         $id = $request->id;
         Customer::find($id)->delete();
         return response()->json(['status'=>true,'message'=>"Customer successfully deleted."]);
     }
}
