<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use OhMyBrew\ShopifyApp\Models\Shop;
use App\SerialCode;
use App\TraitCustomer;
use App\Product;
use App\CustomerGroup;
use App\Exports\SerialcodeExport;
use App\SpecificCustomer;
use OhMyBrew\BasicShopifyAPI;
use DB;
use App\EmailTemplate;
use App\Settings;
use App\UseSerialCode;
use Illuminate\Routing\Route;
use App\Customer;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\SerialcodeRequest;

class SerialCodeController extends Controller
{

  public function __construct()
    {
        $this->obProduct = new Product();
        $this->obCustomerGroup = new CustomerGroup();
        $this->obSpecificCustomer = new SpecificCustomer();
        $this->customrTraint  = new TraitCustomer();    
    }

    public function export() 
    {
        return Excel::download(new SerialcodeExport, 'serialcode.xlsx');
    }

   public function index() {
      $shop = \ShopifyApp::shop();
     	$offset = 0;
      $perpage = 20;
      $currentpage = 1;
      $obSerialcode = new SerialCode();
      $count = SerialCode::where('shop_id','=',$shop->id)->orderBy('created_at', 'desc')->count();
      $totalpages = ceil($count / $perpage);
      $serialcode = $obSerialcode->getSerialcode($shop->id,$offset,$perpage);
      $query = ""; 
      $countSerialcode = count($serialcode);  

      return view('serialcode.index', compact('serialcode', 'query', 'offset', 'perpage', 'currentpage', 'totalpages','countSerialcode'));
    }
   
    public function create(Route $route) { 
        $routename = $route->getName(); 
        return view('serialcode.create',compact('routename'));
    }

    public function store(Request $request) {
      $serialspite = preg_split('/\s+/', $request->serial_code);
      $request->request->add(['serial_code' => $serialspite]); 
      $request->merge(['serial_code' => $serialspite]); 


       $validator = Validator::make($request->all(), [
                  'serial_code.*' => [
                  'required',
                  'unique:serial_code,serial_code',
                  'distinct',
                  'max:255',
               ],
               'products.*' => 'required|max:255',
               'customer_eligibility' => 'required|max:255',
               'start_date' => 'required',
               'customer_eligibility' => 'required|max:255',
            ]);
       
        if ($validator->fails()) {          
            $message = $validator->messages();
            return redirect('serialcode/create')
                        ->withErrors($message)
                        ->withInput();             
        }
     if(count($request->serial_code) > 0){
      foreach ($request->serial_code as $value) {
        $isEndDate = "0";
      if(isset($request->is_end_date) && !empty($request->is_end_date)){
          $isEndDate =  $request->is_end_date;
      }
       

       $limit_one_per_user = ($request->limit_one_per_user == '') ? '0' : '1';
       $limit_number_of_time = ($request->limit_number_of_time == '') ? '0' : '1'; 
        $shop = \ShopifyApp::shop();
        $serialcode = new SerialCode();
        $serialcode->serial_code = $value;
        $serialcode->customer_eligibility = $request->customer_eligibility;
        $serialcode->start_date = $request->start_date;
        $serialcode->start_time = $request->start_time;
        $serialcode->end_date = $request->end_date; 
        $serialcode->is_end_date = $isEndDate;
        $serialcode->limit_number_of_time = $limit_number_of_time;
        $serialcode->limit_one_per_user = $limit_one_per_user; 
        $serialcode->limit_number_user = $request->limit_number_user;  
                
        $serialcode->shop_id = $shop->id;
        $serialcode->save();
      
        $isRegiser = 0;
        if(!empty($request->products)){
          $this->obProduct->Productstore($request->products,$serialcode->id);
        }

        if(!empty($request->customer_group_list) && $request->customer_eligibility == '2'){
          $isRegiser = 1;
          $this->obCustomerGroup->CustomerGroup($request->customer_group_list,$serialcode->id);
        }
        if(!empty($request->specific_customer) && $request->customer_eligibility == '3'){
          $isRegiser = 1;
          $this->obSpecificCustomer->SpecificCustomer($request->specific_customer,$serialcode->id);
        }
       $serialcode->is_register = $isRegiser;
       $serialcode->save();
     }
     return redirect('/serialcode')->with('success', 'Serial code has been created!');
   }  
}

    public function edit(Route $route,$id){
        $serialcode = SerialCode::with(['shop','product','CustomerGroup','SpecificCustomer'])->where('id','=',$id)->first();
        return view('serialcode.create',compact('serialcode','id')); 
    }

    public function update(Request $request,$id){
    $this->validate($request, [ 
             'serial_code' => [
                  'required',
                  'unique:serial_code,serial_code,' . $id,
                  'distinct',
               ],
    		    'customer_eligibility' => 'required|max:255',
            'start_date' => 'required',
        ]);

         $isEndDate = 0;
        if(isset($request->is_end_date) && !empty($request->is_end_date)){
            $isEndDate =  $request->is_end_date;
        }
        $limit_one_per_user = ($request->limit_one_per_user == '') ? '0' : '1';
        $limit_number_of_time = ($request->limit_number_of_time == '') ? '0' : '1';

        $serialcode = SerialCode::find($id);
        $shop = \ShopifyApp::shop();
        $serialcode->serial_code = $request->serial_code;
        $serialcode->customer_eligibility = $request->customer_eligibility;
        $serialcode->limit_number_of_time = $limit_number_of_time;
        $serialcode->limit_number_user = $request->limit_number_user;        
        $serialcode->limit_one_per_user = $limit_one_per_user;
        $serialcode->start_date = $request->start_date;
        $serialcode->start_time = $request->start_time;
        $serialcode->end_date = $request->end_date; 
        $serialcode->is_end_date = $isEndDate;         
        $serialcode->shop_id = $shop->id;
        $serialcode->save();
        $isRegiser = 0;
        $obProduct = new Product();
        if(!empty($request->products)){
           $this->obProduct->Productstore($request->products,$id);
        }else{
          DB::table("products")->where('serial_code_id','=',$id)->delete();
        }

        if(!empty($request->customer_group_list) && $request->customer_eligibility == '2'){
          $isRegiser = 1;
          CustomerGroup::whereNotIn('id', array_column($request->customer_group_list, 'id'))->delete();
          $this->obCustomerGroup->CustomerGroup($request->customer_group_list,$id);
        }else{
           CustomerGroup::where('serial_code_id', $id)->delete();        
        }

        if(!empty($request->specific_customer) && $request->customer_eligibility == '3'){
          $isRegiser = 1;
          SpecificCustomer::whereNotIn('id', array_column($request->specific_customer, 'id'))->delete();
            $this->obSpecificCustomer->SpecificCustomer($request->specific_customer,$id);            
        }else{
          SpecificCustomer::where('serial_code_id', $id)->delete();
        }
       $serialcode->is_register = $isRegiser;
       $serialcode->save();
        return redirect('/serialcode')->with('success', 'Serial code has been updated!');
    }

  public function search(Request $request){
     $obSerialCode = new SerialCode();
     $serialcode =  $obSerialCode->Serialcodefilter($request->all());
      return $serialcode;   
  }  

   public function deleteMultiple(Request $request){
       $ids = $request->ids;
       SpecificCustomer::whereIn('serial_code_id',explode(",",$ids))->delete();
       CustomerGroup::whereIn('serial_code_id',explode(",",$ids))->delete();
       Product::whereIn('serial_code_id',explode(",",$ids))->delete();
       SerialCode::whereIn('id',explode(",",$ids))->delete();SerialCode::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['status'=>true,'message'=>"Serial code deleted successfully."]);   
    }
     public function Singledelete(Request $request){
      Product::where('serial_code_id',$request->ids)->delete();
      SpecificCustomer::where('serial_code_id',$request->ids)->delete();
      CustomerGroup::where('serial_code_id',$request->ids)->delete();
      SerialCode::where('id',$request->ids)->delete();
        return response()->json(['status'=>true,'message'=>"Serial code deleted successfully."]);   
    } 

    public function activecode(Request $request){
      $message =  '';
      foreach ($request->ids as $value) {
         SerialCode::where('id', $value)
          ->update(['status' => $request->flag]);         
      }
       if($request->flag == 0){
           $message =  "Serial code Actived successfully."; 
        }else{
          $message =  "Serial code InActived successfully.";
        }
       return response()->json(['status'=>true,'message'=> $message]);
    }   

    public function applycode(Request $request){

      $shopifyrequerst = $request['applycodecode'];
      $requesrexpload =  explode("^", $shopifyrequerst);
      $codeArray = array();
      foreach ($requesrexpload as $exploadvalue) {
         $loopvalexpload =  explode("=", $exploadvalue);
         $codeArray[$loopvalexpload[0]] = $loopvalexpload[1];
      }
      $shop = $this->getshop($codeArray['domain']);
      $serial = $codeArray['serialcode']; 
  $error['error'] = '';
  if(!empty($shop)){
     
    $checkserialcode = $serialcode = SerialCode::where('shop_id','=',$shop->id)->where('serial_code','=',$codeArray['serialcode'])->first();
     $product = array();
     
     if(!empty($checkserialcode)){
      if($checkserialcode->status == 1){
        $error['error'] = 'This serialcode is InActive.';
        return response()->json($error); 
      }
     $serialcodeActiveDate =  $this->checkactiveDate($checkserialcode->start_date);
     if($serialcodeActiveDate == false){
      $error['error'] = 'SerialCode not Activate yet.';
        return response()->json($error); 
     }       
            $productget = Product::where('serial_code_id','=',$checkserialcode->id)->get();
             $counter = 0;
              if(count($productget) > 0){
                  foreach ($productget as $productvalue) {
                     $product[$counter]['id'] =  $productvalue->id;
                     $product[$counter]['shopify_product_id'] =  $productvalue->product_id;
                     $product[$counter]['product_title'] =  $productvalue->product_title;
                     $counter++;
                  }
              }else{
               $error['error'] = 'Product not found.';
               return response()->json($error); 
              }
       }else{
               $error['error'] = 'Invalid serialcode.';
               return response()->json($error);
       }
  }else{
         $error['error'] = 'Invalid shop.';
        return response()->json($error);       
  }
      return response()->json($product); 
    }

  public function checkactiveDate($activedate){
    
    $currentDate = date('Y-m-d');
      if ($currentDate >= $activedate){
           return true;
      }else{
         return false;
      }
    }


    public function getshop($domain){
        return $shop = Shop::where('shopify_domain','LIKE', '%'.$domain.'%')->first();
    }

    public function checkserialcode(Request $request){ 
 
      $shopifyrequerst = $request['customerverify'];
      $requesrexpload =  explode("^", $shopifyrequerst);
      $customerarray = array();
      foreach ($requesrexpload as $exploadvalue) {

         $loopvalexpload =  explode("=", $exploadvalue);
         $customerarray[$loopvalexpload[0]] = $loopvalexpload[1];
      }
    $shop = $this->getshop($customerarray['domain']);
    $customerId = '';
    $shopCustomer = Customer::where('email','=',$customerarray['user_mail'])->first();

    $customerId = '';
    if(!empty($shopCustomer)){
        $customerId = $shopCustomer->id;
        $this->customrTraint->customertagpdate($shopCustomer->shopify_customer_id,$shop,$customerarray['product_id']);

     }else{ 
                $obCustomer = new Customer();
                $obCustomer->first_name =  $customerarray['firstname'];
                $obCustomer->last_name =  $customerarray['lastname'];  
                $obCustomer->email =  $customerarray['user_mail'];
                $obCustomer->shop_id =  $shop->id; 
             $CustomerSearch = $this->customrTraint->customersearch($shop,$customerarray['user_mail']);
             if(!empty($CustomerSearch)){ 
              /**************New customer create in our app***/
                  $customerIdexpload =  explode("/", $CustomerSearch->id);
                  $obCustomer->shopify_customer_id =  $customerIdexpload[4];  
                  $obCustomer->save();
                  $customerId = $obCustomer->id;
                  $this->customrTraint->customertagpdate($customerId,$shop,$customerarray['product_id']); 
             }else{
              /**************New customer create in shopify and out app***/
               $CustomerCreate = $this->customrTraint->customercreate($shop,$customerarray);
               $customerIdexpload =  explode("/", $CustomerCreate->id);
               $customerId = $customerIdexpload[4]; 
               $obCustomer->shopify_customer_id =  $customerIdexpload[4]; 
               $obCustomer->save();
               $customerId = $obCustomer->id;
               $this->customrTraint->customertagpdate($customerIdexpload[4],$shop,$customerarray['product_id']);
             }
     }  

     if(!empty($customerarray['serialcode'])){ 
     
         $isvarify = $this->verifySerialcode($customerId,$customerarray['serialcode'],$shop);
         $status = '';
            if(empty($isvarify['error'])){
               if(!empty($isvarify['customerId'])){
                  $obUseSerialCode = new UseSerialCode();
                  $obUseSerialCode->customer_id = $isvarify['customerId'];
                  $obUseSerialCode->shop_id = $shop->id; 
                  $obUseSerialCode->product_id = $customerarray['product_id'];
                  $obUseSerialCode->serial_id = $isvarify['serial_id'];
                  $obUseSerialCode->save();
                  $to = $customerarray['user_mail'];
                  $this->sendMail($to,$customerarray,$shop->id);
                  $status = '1';
                }else{
                   $status = '0';
                }
              }else{
                $status = $isvarify['error'];
              }               
              return response()->json($status);  
           }     
    }
    function sendMail($to,$customerarray,$shop){
      $emailTemplate = EmailTemplate::where('shop_id','=',$shop)->first();
      $subject = "Serialcode notification";
      $content = 'Welcome to the Our Store.';
      if(!empty($emailTemplate)){
        $subject = $emailTemplate->subject;
        $content = $emailTemplate->content;
      }
       $settings = Settings::where('shop_id','=',$shop)->first();
                       if(!empty($settings) && $settings->send_notification == '0'){
                           \Mail::to($to)->send(new \App\Mail\SerialcodeNotification($customerarray,$subject,$content));
                       }else{
                         \Mail::to($to)->cc(env('MAIL_USERNAME'))->bcc(env('MAIL_USERNAME'), 'Serial Notification')->send(new \App\Mail\SerialcodeNotification($customerarray,$subject,$content));
                       }
                  
              return true;    
      }
    
    public function verifySerialcode($customerId,$serialcode,$shop){ 
      $isvarify = array();
      $checkserialcode = $serialcode = SerialCode::with(['shop', 'product','CustomerGroup','SpecificCustomer'])->where('shop_id','=',$shop['id'])->where('serial_code','=',$serialcode)->first();
    
     if(!empty($checkserialcode)){
      $serialcodederail = DB::table('use_serial_codes')                           
                           ->where('use_serial_codes.customer_id','=',$customerId)
                           ->where('use_serial_codes.serial_id','=',$checkserialcode->id)
                           ->count();
    $TotalUseage = DB::table('use_serial_codes')                           
                           ->where('use_serial_codes.serial_id','=',$checkserialcode->id)
                           ->count();

      $checkLimit = $this->checkUsageLimit($serialcodederail,$checkserialcode->limit_number_user,$checkserialcode->limit_number_of_time,$checkserialcode->limit_one_per_user,$TotalUseage);

      if($checkLimit == ''){
        if($checkserialcode->customer_eligibility == 2){    
          $GroupDetail = $checkserialcode->CustomerGroup;
          $AllCustomer = array();
          $Groupcount = 0;
          $GroupId = '';
          foreach ($GroupDetail as $Groupvalue) {
             $CustomerGroup = $this->customrTraint->customerGroupcheck($shop,$Groupvalue->customer_group_id,$customerId);
             if(!empty($CustomerGroup)){
                $isvarify['customerId'] = $customerId;
                $isvarify['serial_id'] = $checkserialcode->id;
                $GroupId = $CustomerGroup;
             }           
          }
          if(empty($GroupId)){
            $isvarify['error'] = "You are not authorise use this serial code."; 
              return $isvarify;
          }  
         }else if($checkserialcode->customer_eligibility == 3){
                $specificCustomer = $this->customrTraint->specificcustomercheck($customerId);
              if(!empty($specificCustomer)){
                $isvarify['customerId'] = $customerId;
                $isvarify['serial_id'] = $checkserialcode->id;
             }else{
               $isvarify['error'] = "You are not authorise use this serial code."; 
                  return $isvarify;
             }   
               
         }else{
              $isvarify['customerId'] = $customerId;
              $isvarify['serial_id'] = $checkserialcode->id;
         }
      $isvarify['error'] = ''; 
      }else{
        $isvarify['error'] = $checkLimit; 
        return $isvarify;
      }                  
     } 
        return $isvarify;         
    }

    public function checkUsageLimit($count,$limit_number_user,$limit_number_of_time,$limit_one_per_user,$TotalUseage){

  $Usagelimit = ($limit_number_user != '') ? $limit_number_user : '';
  $error = '';

      if($limit_number_of_time == '1' && $limit_one_per_user == '1'){
        if(!empty($Usagelimit)){
           if($count >= 1 && $TotalUseage >= $Usagelimit){
              $error = 'Only once use this code.';
           }
         }else{
          if($count >= 1){
              $error = 'Only once use this code.';
           }
         }
          
      }else if($limit_one_per_user == '1' && $limit_number_of_time == '0'){
        if($count >= 1){
              $error = 'Only once use this code.';
           }
      }else if($limit_one_per_user == '0' && $limit_number_of_time == '1'){
         if(!empty($Usagelimit)){
           if($TotalUseage >= $Usagelimit){
              $error = 'This serial code limit has been over.';
           }
         }
      }
      return $error;
    }        
}
