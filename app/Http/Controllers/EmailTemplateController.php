<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\EmailTemplate;

class EmailTemplateController extends Controller
{
     public function create()
    { 
    	$shop = \ShopifyApp::shop();
    	$emailemplate = EmailTemplate::where('shop_id','=',$shop->id)->first();
    	return view('emailtemplate.create', compact('emailemplate'));
    }

    public function store(Request $request){
    	 $this->validate($request, [
            'content' => 'required',
            'subject' => 'required',
        ]);

    	 $shop = \ShopifyApp::shop();
         $EmailTemplate = EmailTemplate::where('shop_id','=',$shop->id)->first();
    	 if(!empty($EmailTemplate)){
    	 	$EmailTemplate->content = $request->content;
    	 	$EmailTemplate->subject = $request->subject;
    	 	$EmailTemplate->shop_id = $shop->id;
    	 	$EmailTemplate->save();
    	 }else{
    	 	$obEmailTemplate = new EmailTemplate();
    	 	$obEmailTemplate->content = $request->content;
    	 	$obEmailTemplate->subject = $request->subject;    	 
    	 	$obEmailTemplate->shop_id = $shop->id;
    	 	$obEmailTemplate->save();
    	 }
    	return Redirect::back()->with('success', 'Email Template has been save successfully!');  
    }
}
