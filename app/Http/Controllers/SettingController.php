<?php

namespace App\Http\Controllers;
use App\Settings;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function create(){
        $shop = \ShopifyApp::shop();
        $settings = '';
        $ObSettings = Settings::where('shop_id','=',$shop->id)->first();
         if(!empty($ObSettings)){
              $settings = $ObSettings;
         }
         return view('settings.create',compact('settings'));
    }

    public function store(Request $request){
    
     $sendnotification = '0';
     if(!empty($request->send_notification)){
        $sendnotification = $request->send_notification;
     }
        $shop = \ShopifyApp::shop(); 

         $ObSettings = Settings::where('shop_id','=',$shop->id)->first();
         if(empty($ObSettings)){
              $ObSettings = new Settings();
         }
        $ObSettings->send_notification = $sendnotification;
        $ObSettings->shop_id = $shop->id;
        $ObSettings->save(); 
        return Redirect::back()->with('success', 'Setting has been save successfully!');     
    }
}
