<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SerialcodeNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public function __construct($serialcode,$subject,$content) {
        //
        $this->serialcode = $serialcode;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->view('serialcode.SerialcodeNotification')->with([
                    'firstname' => $this->serialcode['firstname'],
                    'lastname' => $this->serialcode['lastname'],
                    'serialcode' => $this->serialcode['serialcode'],
                    'subject' => $this->subject,
                    'content' => $this->content
        ]);
    }
}
