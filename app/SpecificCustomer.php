<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificCustomer extends Model
{
    protected $table = 'specific_customer';
    protected $fillable = [
        'specific_customer_id','serial_code_id','customer_name',
    ];

    public function serialcode()
    {
        return $this->belongsTo('App\SerialCode');
    }
    
    public function SpecificCustomer($request,$id){
      	foreach ($request as $specificcustomer) {
     	    $obSpecificCustomer  = (isset($specificcustomer['id'])) ? SpecificCustomer::find($specificcustomer['id']) : new SpecificCustomer;
    	    $obSpecificCustomer->specific_customer_id =  $specificcustomer['customer_id']; 
            $obSpecificCustomer->firstname =  $specificcustomer['firstname'];
            $obSpecificCustomer->lastname =  $specificcustomer['lastname'];
            $obSpecificCustomer->email =  $specificcustomer['email'];
            $obSpecificCustomer->serial_code_id =  $id;
            $obSpecificCustomer->save();
        }
    }
}
