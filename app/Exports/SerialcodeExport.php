<?php

namespace App\Exports;

use App\SerialCode;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SerialcodeExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {      
        $SerialCode = SerialCode::all();        
        return $SerialCode;
    }

     public function headings(): array
    {
        return [
            'Id',
            'Shop id',
            'SerialCode',
            'Customer eligibility',
            'User limit',
            'Start date',
            'End date',
            'Start time',
            'Is end date flag',
            'status',
            'Create Date',            
            'Update Date',
        ];
    }
}
