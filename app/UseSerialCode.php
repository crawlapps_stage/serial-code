<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class UseSerialCode extends Model
{
   protected $table = 'use_serial_codes';

    protected $fillable = [
        'customer_id','shop_id', 'product_id', 'serial_id'
    ];    

 public function serialcode()
    { 
        return $this->belongsTo('App\SerialCode');
    }
}
