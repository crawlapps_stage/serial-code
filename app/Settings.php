<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'send_notification','shop_id'
    ];

     public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

 public function Settingsfilter($request){

 	$shop = \ShopifyApp::shop(); 
    $keyword = $request['search'];
    $action = $request['action'];
    $perpage = $request['perpage'];
    $lastItem = ($action == 'search') ? 0 : $request['lastItem'];

     $settingssquery = Settings::with('shop');
        $settingssquery->where(function($query1) use ($keyword) {
            $query1->where('name', 'like', '%' . $keyword . '%');
            $query1->orwhere('value', 'like', '%' . $keyword . '%');
        });
        $settingssquery->where('shop_id', '=', $shop->id);
        $settings = $settingssquery->orderBy('id', 'asc')->skip($lastItem)->take($perpage)->get();

   if ($action == 'search') {
            $lastItem = 0;
            $count = $settingssquery->count();
            $totalpages = ceil($count / $perpage);
            $response = [
                'settings' => $settings,
                'count' => $count,
                'totalpages' => $totalpages
            ];
            return json_encode($response);
        } else {
            return json_encode($settings);
        }     
 }
}
