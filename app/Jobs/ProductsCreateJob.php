<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OhMyBrew\ShopifyApp\Models\Shop;
use Carbon\Carbon;
use App\Product;

class ProductsCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
     public function handle()
    {
        $this->shop = Shop::where('shopify_domain', $this->shopDomain)->first();
        $request = json_decode(json_encode($this->data), true);
        $Image = '';
        if(isset($request['images'][0]['src'])){
            $Image = $request['images'][0]['src'];
        }
        
        foreach ($request['variants'] as $variants) {

            $obProduct = new Product();
            $obProduct->product_id = $request['id'];
            $obProduct->product_name = $request['title'];
            $obProduct->variant_id = $variants['id'];
            $obProduct->variant_name = $variants['title'];
            $obProduct->inventory_item_id = $variants['inventory_item_id'];
            $obProduct->sku = $variants['sku'];
            $obProduct->vendor_name = $request['vendor'];
            $obProduct->image = $Image;
            $obProduct->barcode = $variants['barcode'];
            $obProduct->inventory_quantity = $variants['inventory_quantity'];
            $obProduct->price = $variants['price'];

            $GetInventory = $this->shop->api()->rest('GET', '/admin/inventory_items/' . $variants['inventory_item_id'] . '.json');
            $Inventorydetail = $GetInventory->body;
            if (!empty($Inventorydetail->inventory_item->cost)) {
                 $cost = number_format($Inventorydetail->inventory_item->cost,2, '.', '');  
            } else {
                $cost = '';
            }
            $obProduct->cost = $cost;
            $obProduct->shop_id = $this->shop->id;
            $obProduct->save();
        }
      
    }
}
