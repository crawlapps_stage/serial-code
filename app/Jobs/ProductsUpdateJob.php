<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use OhMyBrew\ShopifyApp\Models\Shop;
use App\Product;

class ProductsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->shop = Shop::where('shopify_domain', $this->shopDomain)->first();
        $request = json_decode(json_encode($this->data), true);

        $AllProduct = Product::where('product_id', '=', $request['id'])->get();
        $VariantId = array();
        $counter = 0;
        $Image = '';
        if(isset($request['images'][0]['src'])){
            $Image = $request['images'][0]['src'];
        }

        foreach ($request['variants'] as $variants) {
            $getProduct = Product::where('product_id','=',$request['id'])->where('variant_id','=',$variants['id'])->first();
                if(empty($getProduct)){
                $obProduct = new Product();
                $obProduct->product_id = $request['id'];
                $obProduct->product_name = $request['title'];
                $obProduct->variant_id = $variants['id'];
                $obProduct->variant_name = $variants['title'];
                $obProduct->vendor_name = $request['vendor'];
                $obProduct->sku = $variants['sku'];
                $obProduct->image = $Image;
                $obProduct->barcode = $variants['barcode'];
                $obProduct->inventory_quantity = $variants['inventory_quantity'];
                $obProduct->price = $variants['price'];
                $obProduct->inventory_item_id = $variants['inventory_item_id'];
                $obProduct->cost = $this->Cost($variants['inventory_item_id']);
                $obProduct->shop_id = $this->shop->id;
                $obProduct->save();
            }else{
                $getProduct->product_id = $request['id'];
                $getProduct->product_name = $request['title'];
                $getProduct->variant_id = $variants['id'];
                $getProduct->variant_name = $variants['title'];
                $getProduct->sku = $variants['sku'];
                $getProduct->vendor_name = $request['vendor'];
                $getProduct->image = $Image;
                $getProduct->barcode = $variants['barcode'];
                $getProduct->inventory_quantity = $variants['inventory_quantity'];
                $getProduct->price = $variants['price'];
                $getProduct->cost = $this->Cost($variants['inventory_item_id']);
                $getProduct->inventory_item_id = $variants['inventory_item_id'];
                $getProduct->shop_id = $this->shop->id;
                $getProduct->save();
            }
            $VariantId[$counter] = $variants['id'];
            $counter++;
        }
        foreach ($AllProduct as $value) {
                if (!in_array($value->variant_id, $VariantId)) {
                    $this->VariantDelete($value->variant_id, $value->product_id);
                }
            }
    }

     public function VariantDelete($variantID,$Product_id)
    {
         $Product = Product::where('product_id','=',$Product_id)->where('variant_id','=',$variantID)->first();
         if(!empty($Product)){
              $Product->delete();
         }
    }

    public function Cost($inventory_item_id)
    {
        $GetInventory = $this->shop->api()->rest('GET', '/admin/inventory_items/' . $inventory_item_id . '.json');
        $Inventorydetail = $GetInventory->body;
            $cost = '';
             if (!empty($Inventorydetail->inventory_item->cost)) {
                $cost = number_format($Inventorydetail->inventory_item->cost,2, '.', '');
             }
        return $cost;
    }
}
