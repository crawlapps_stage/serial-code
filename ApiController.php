<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriesResources;
use App\Http\Resources\FlyerResources;
use App\Http\Resources\ParentFlyerResources;

use App\Category;
use App\Flyer;
use App\ParentFlyer;

class ApiController extends Controller
{
    public function getCategories()
    {
        $entities = Category::all();
        return response()->json([
            "status"=> true,
            "message"=> "Category list get successfully.",
            'data' => CategoriesResources::collection($entities)
        ], 200);
    }

    public function getFlyers(Request $request)
    {
        $entities = ParentFlyer::with(['flyers'])->where('category_id', $request->category_id)->get();
        return response()->json([
            "status"=> true,
            "message"=> "Flyer list get successfully.",
            'data' => ParentFlyerResources::collection($entities)
        ], 200);
    }

    public function getSubFlyers(Request $request)
    {
        $entities = Flyer::where('parent_flyer_id', $request->flyer_id)->orderBy('order_by')->get();
        return response()->json([
            "status"=> true,
            "message"=> "Sub Flyer list get successfully.",
            'data' => FlyerResources::collection($entities)
        ], 200);
    }
}
