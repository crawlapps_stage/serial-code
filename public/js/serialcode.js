var host = "https://serialcode.test";
var domain = document.getElementById('domain').value;
var request = new XMLHttpRequest();
var validemail = 0;
   request.open('GET', host+'/customerform/'+domain, true)
        request.onload = function () {
            var data = JSON.parse(this.response)
            document.getElementById("crawlapp_serialcode_app").innerHTML= data;
    }
        request.send();
function submitform(){
  remove_message();
  if(domain == "")
   {  document.getElementById("domain_err").innerHTML="Please set the proper short code.";
             return false;  }
  if(validemail == 0){
      var serialcode= document.getElementById('serialcode').value; 
          if(serialcode=="")
              {
               document.getElementById("serialcode_err").innerHTML="Please enter the serial code";
               return false;
             }
        document.getElementById("serialcode_err").innerHTML="";
        /************Start Appy serialcode Function******/           
        ApplyCode(serialcode);
        /************End Appy serialcode Function*******/         
        
  }else{
    var firstname = document.getElementById('firstname').value;
    var lastname = document.getElementById('lastname').value;;
    var user_mail = document.getElementById('user_mail').value;
    var serialcode = document.getElementById('serialcode').value; 
    var product_id = document.getElementById('product_id').value;
    var errormessage = [];
        if(user_mail=="")
              {
               document.getElementById("email_err").innerHTML="Please enter the email.";
               errormessage = [1];
               
             }
             if(firstname=="")
              {
               document.getElementById("firstname_err").innerHTML="Please enter the first name.";
               errormessage = [1];
             }
             if(lastname=="")
              {
               document.getElementById("lastname_err").innerHTML="Please enter the last name.";
               errormessage = [1];
             } 

             if(errormessage.length > 0){
                 return false;
             }else{
             document.getElementById("firstname_err").innerHTML="";
             document.getElementById("lastname_err").innerHTML="";
             document.getElementById("email_err").innerHTML="";
             }

    var formrequest = new XMLHttpRequest();
  
   formrequest.open('POST', host+'/serialcodefront/checkserialcode', /* async = */ true);    
         formrequest.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var data = JSON.parse(formrequest.response);
            if(data == 1){
              document.getElementById("successr_massage").style.display = '';
              document.getElementById("serialcode_suceess_message").innerHTML= "Serial code successfully applied.";
              document.getElementById("serialcode_message").innerHTML= ""; 
              document.getElementById("serialcode").value = '';  
              document.getElementById("message").innerHTML= "";
              document.getElementById("mail_div").style.display = 'none';
              validemail = 0;
             }else if(data == 0){
              document.getElementById("danger_massage").style.display = '';
              document.getElementById("serialcode_message").innerHTML= "The serial code is invalid.";
              document.getElementById("serialcode_suceess_message").innerHTML= "";              
             }else{
              document.getElementById("danger_massage").style.display = '';
              document.getElementById("serialcode_message").innerHTML= data;              
              document.getElementById("serialcode_suceess_message").innerHTML= "";              
             }   
        }
        
        formhidesshow('none');
        validemail = '0'; 
         }
         formrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
         formrequest.send("customerverify="+
               "firstname="+firstname+
               "^lastname="+lastname+
               '^user_mail='+user_mail+
               '^serialcode='+serialcode+            
               '^product_id='+product_id+
               '^domain='+domain);    
  }    
}

function remove_message(){
 document.getElementById("successr_massage").style.display = 'none';
 document.getElementById("danger_massage").style.display = 'none';
}

function ApplyCode(serialcode){ 
     var applycoderequest = new XMLHttpRequest();
     applycoderequest.open('POST', host+'/serialcodefront/applycode', /*async = */ true);    
         applycoderequest.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var productdetail = JSON.parse(applycoderequest.response);
        var displaytext = '';
        if (typeof productdetail['error'] === "undefined" || productdetail['error'] == '') {
           displaytext = "";
           $("option").remove();
            buttondisable();
            validemail = 1;
            for (let i=0; i<productdetail.length; i++) {               
                var option = document.createElement("option");
                    option.text = productdetail[i].product_title;
                    option.value = productdetail[i].shopify_product_id;
                    var select = document.getElementById("product_id");
                    select.appendChild(option);
                    document.getElementById("message").innerHTML= '';
              }
            }else {
              validemail = 0;
              $("option").remove();
              var massage = "Product not found";
              var option = document.createElement("option");
              option.text = massage;
              var select = document.getElementById("product_id");
              select.appendChild(option);
               displaytext = "none";
              document.getElementById("danger_massage").style.display = '';
              document.getElementById('serialcode_message').innerHTML = productdetail['error'];
            } 
            formhidesshow(displaytext);
            document.getElementById("serialcode_suceess_message").innerHTML= ""; 
        } 
   }
         applycoderequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
         applycoderequest.send("applycodecode=serialcode="+serialcode+
               "^domain="+domain); 
}

function formhidesshow(displaytext){

        document.getElementById("firstname").value = '';
        document.getElementById("lastname").value = '';
        document.getElementById("user_mail").value = '';
        document.getElementById("form_firstname").style.display = displaytext;
        document.getElementById("form_lastname").style.display = displaytext; 
        document.getElementById("form_email").style.display = displaytext;
        document.getElementById("form_product").style.display = displaytext;
           
} 
function buttondisable(){
  var serialcode = document.getElementById('serialcode').value; 
  var user_mail = document.getElementById('user_mail').value; 
    if(user_mail != '' && serialcode != '' && validemail == 1){
      //document.getElementById("submit_button").disabled = false;
    }else{
      //document.getElementById("submit_button").disabled = true;
    }
}


function ValidateEmail(mail) 
{
  var user_mail = document.getElementById('user_mail').value; 
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user_mail))
  {
    validemail = 1;    
  }else{
    validemail = 0;
  }  
  buttondisable();
}