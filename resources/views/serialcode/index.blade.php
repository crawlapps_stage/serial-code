 @extends('layouts.master')

@section('content')

<div class="Polaris-Page">
            <div class="Polaris-Page-Header Polaris-Page-Header--hasNavigation Polaris-Page-Header--hasActionMenu Polaris-Page-Header--mobileView">
            <div class="Polaris-Page-Header__MainContent">
                <div class="Polaris-Page-Header__TitleActionMenuWrapper">
                <div class="Polaris-Page-Header__Title">
                    <div>
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Serial numbers</h1>
                    <button type="button" class="Polaris-ActionList__Item">
                        <div class="Polaris-ActionList__Content">
                            <div class="Polaris-ActionList__Image"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                <path d="M13.707 6.707a.997.997 0 0 1-1.414 0L11 5.414V13a1 1 0 1 1-2 0V5.414L7.707 6.707a.999.999 0 1 1-1.414-1.414l3-3a.999.999 0 0 1 1.414 0l3 3a.999.999 0 0 1 0 1.414zM17 18H3a1 1 0 1 1 0-2h14a1 1 0 1 1 0 2z"></path>
                                </svg></span></div>

                            <div class="Polaris-ActionList__Text">
                                <a class="btn btn-warning" onClick="CSV_data('CSV')">Export file</a></div>
                        </div>
                    </button>
                    </div>
                    
                </div>
                <div class="Polaris-Page-Header__ActionMenuWrapper">
                    <button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">
                        <a href="{{route('serialcode.create')}}" style="color:white; text-decoration: none;">
                        Add Code </a> </span></span></button>
                </div>
                </div>
            </div>
            </div>
            <div class="Polaris-Page__Content">
                <div class="Polaris-Card">
                    <div>
                        <input type="hidden" value="" id="serialcode_list_status" name="serialcode_list_status">
                        <ul role="tablist" class="Polaris-Tabs">
                            <li role="presentation" class="Polaris-Tabs__TabContainer">
                                <button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers">
                                    <span class="Polaris-Tabs__Title active"  at="0" value="{{ request('serialcodelist') }}" id="serialcoderegistertastus">All</span>
                                </button>
                            </li>   
                             <li role="presentation" class="Polaris-Tabs__TabContainer">
                                <button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers">
                                    <span class="Polaris-Tabs__Title" at="1" value="{{ request('serialcodelist') }}" id="serialcoderegistertastus">Registered</span>
                                </button>
                            </li> 
                             <li role="presentation" class="Polaris-Tabs__TabContainer">
                                <button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers">
                                    <span class="Polaris-Tabs__Title" at="2" value="{{ request('serialcodelist') }}" id="serialcoderegistertastus">Unregistered</span>
                                </button>
                            </li>                           
                        </ul>
                        <div class="Polaris-Tabs__Panel" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
                            <div class="Polaris-Card__Section">
                                <div class="">
                                    <div class="Polaris-Filters">
                                        <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                            <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                              
                                                <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div class="Polaris-Labelled--hidden">
                                                            <div class="Polaris-TextField">
                                                                <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                    <span class="Polaris-Filters__SearchIcon">
                                                                        <span class="Polaris-Icon">
                                                                            <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <input id="search_code" placeholder="Filter customers" value="{{ request('search') }}" onkeyup="filter_data('search')" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="serial-list-section" id="serial-list-section">
                                    <div class="serial-list-multiselect">
                                        <div class="Polaris-Page-Header--separator serial-list-header">
                                            <label class="Polaris-Choice check-app-serial-code" for="check_all">
                                                <span class="Polaris-Choice__Control">
                                                    <span class="Polaris-Checkbox">
                                                        <input id="check_all" type="checkbox" class="Polaris-Checkbox__Input">
                                                        <span class="Polaris-Checkbox__Backdrop"></span>
                                                        <span class="Polaris-Checkbox__Icon">
                                                            <span class="Polaris-Icon">
                                                                <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                    <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span> 
                                                <span class="Polaris-Choice__Label" id="list_select_serial_code_show_code">Showing {{ $countSerialcode }} discount code</span>
                                            </label>
                                            <div class="multiselect-row">
                                            <div class="multiselect-item">
                                                <div class="multiselect-checkbox">
                                                    <label class="Polaris-Choice" ><span class="Polaris-Choice__Control" for="check_all_label"><span class="Polaris-Checkbox">
                                                    <input id="check_all_label" type="checkbox" class="Polaris-Checkbox__Input check_box_check"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
              <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
            </svg></span></span></span></span><span class="Polaris-Choice__Label" id="list_select_serial_code"> </span></label>
                                                </div>
                                                <div class="custom-dropdwon" >
                                                    <button id="slideToggleaction" type="button" class="Polaris-Button" tabindex="0" aria-controls="Popover1" aria-owns="Popover1" aria-haspopup="true" aria-expanded="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Action</span><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg></span></span></span></button>
                                                    <div class="custom-dropdwon-ul" style="display: none;">
                                                        <ul>
                                                            <li class="serailcode_status" value="0"><a href="#">Enable discount codes</a></li>
                                                            <li class="serailcode_status" value="1"><a href="#">Disable discount codes</a></li>
                                                            <li class="deleted"><a href="#">Delete discount codes</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                            <div class="select-label">
                                                <div class="Polaris-Labelled__LabelWrapper">
                                                    <div class="Polaris-Label"><label id="Select9Label" for="Select9" class="Polaris-Label__Text">Sorts</label></div>
                                                </div>
                                                 <div class="Polaris-Select">
                                <select class="Polaris-Select__Input" id="sort_select" name="sort" onchange="filter_data('sort')">
                                                        <option value="'created_at', 'desc'"> Last created</option>
                                                        <option value="'serial_code', 'desc'">Serial code(A-Z)</option>
                                                        <option value="'serial_code', 'asc'">Serial code(Z-A)</option>
                                                        <option value="'start_date', 'desc'">Start date (ascending)</option>
                                                        <option value="'start_date', 'asc'">Start date (descending)</option>
                                                       <!--   <option value="'end_date', 'asc'">End date (ascending)</option>
                                                          <option value="'end_date', 'desc'">End date (descending)</option> -->
                                                    </select>
                                                    <div class="Polaris-Select__Content" aria-hidden="true"><span class="Polaris-Select__SelectedOption">Last create</span><span class="Polaris-Select__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                            <path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path>
                                                        </svg></span></span></div>
                                                    <div class="Polaris-Select__Backdrop"></div>
                                                </div> 
                                            </div>
                                        </div>                                        
                                    </div>
                                    <ul class="serial-list" id="serial-list">
                                        <li id="show_li_result" style="display: none;"> <li>
                                       @if (count($serialcode) >= 1)
                              
                                        @foreach($serialcode as $key=>$codeval)
                                       
                                        <li class="table-row">
                                            <div>
                                            <label class="Polaris-Choice" ><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox">
                                             <input  data-id="{{$codeval['id']}}" type="checkbox" class="Polaris-Checkbox__Input checkbox-check" aria-invalid="false" role="checkbox" aria-checked="false" value=""><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                              </svg></span></span></span></span></label>
                                            </div>
                                            <div class="serial-list-text">
                                                <h3 class="Polaris-Subheading">
                                                 <a href="{{route('serialcode.edit',$codeval['id'])}}" style="text-decoration : none; color : #0000FF;">{{ $codeval['serial_code'] }}</a>
                                                  </h3>
                                                  <span class="Polaris-TextStyle--variationSubdued date-serial">
                                                  @php
                                                       echo $codeval['message'];
                                                   @endphp </span> <br>                                                  
                                                   @if ($codeval['status'] == 0)
                                                      <span class="Polaris-Badge Polaris-Badge--statusSuccess" >Active</span>
                                                    @else
                                                     <span style="background-color: red; color: white;" class="Polaris-Badge Polaris-Badge--error" >InActive</span>
                                                  @endif                                                
                                                <div class="Polaris-TextStyle--variationSubdued date-serial">From {{ $codeval['start_date']}}</div>
                                            </div>
                        <div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">
                            <button type="button" data-id="{{$codeval['id']}}" id="single_delete" onclick="deleteItem({{ $codeval['id'] }})" class="Polaris-Button Polaris-Button--destructive"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Delete</span></span></button></div>
                                        </li>
                                         @endforeach
                                         @else
                                         <li style="display:inline-block; text-align: center;width: 100%;" class="table-row">No record available. </li>
                                          @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="pagination_fields">
                                                <input type="hidden" name="perpage" id="perpage" value="{{ $perpage }}" />
                                                <input type="hidden" name="lastItem" id="lastItem" value="{{ $offset }}" />
                                                <input type="hidden" name="totalpages" id="totalpages" value="{{ $totalpages }}" />
                                                <input type="hidden" name="currentpage" id="currentpage" value="{{ $currentpage }}" />
                                            </div>
                                            <div class="order_arrow">
                                                <nav class="Polaris-Pagination" aria-label="Pagination">
                                                    <button type="button" 
                                                            id="btnPrev"
                                                            class="Polaris-Pagination__Button"
                                                            aria-label="Previous">
                                                        <span class="Polaris-Icon">
                                                            <svg
                                                                viewBox="0 0 20 20" class="Polaris-Icon__Svg"
                                                                focusable="false" aria-hidden="true">
                                                                <path
                                                                    d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2"
                                                                    fill-rule="evenodd"></path>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                    <button type="button"
                                                            id="btnNext"
                                                            class="Polaris-Pagination__Button"
                                                            aria-label="Next">
                                                        <span
                                                            class="Polaris-Icon">
                                                            <svg viewBox="0 0 20 20"
                                                                class="Polaris-Icon__Svg" focusable="false"
                                                                aria-hidden="true">
                                                                <path
                                                                    d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414"
                                                                    fill-rule="evenodd"></path>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </nav>
            </div>
        </div>
        @if(session()->get('success'))
                                    <div class="tost_msg">
                                        <input type="hidden" name="texttost" id="texttost" value="{{ session()->get('success') }}" />
                                    </div>
                                    @endif
        @endsection
        

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>



<script type="text/javascript">

function filter_data(action) {    
   sorting = $('#sort_select').val();
   perpage = $('#perpage').val();
   lastItem = $('#lastItem').val();
   Filter = $('#search_code').val();
   registerstatus = $('#serialcode_list_status').val();
            $.ajax({
            type : 'get', 
            url : "{{route('serialcode.search')}}",
            data:{'action':action,'sorting':sorting,'search':Filter,'perpage':perpage, 'lastItem':lastItem, 'registerstatus':registerstatus },
            success:function(data){
                 if(action === 'search'){
                    $('#currentpage').val('1'); //set current page as initial = 1
                    $('#totalpages').val(JSON.parse(data).totalpages);  //set total pages for search criteria
                    serialcode = JSON.parse(data).Serialcode;
                } else{
                    serialcode = JSON.parse(data);
                }
                print_table(serialcode); //print ajax response for pagination
                checkPaginationButtons();                
            }
        });     
}


function CSV_data(action) {    
    sorting = $('#sort_select').val();
   perpage = $('#perpage').val();
   lastItem = $('#lastItem').val();
   Filter = $('#search_code').val();
   registerstatus = $('#serialcode_list_status').val();
            $.ajax({
            type : 'get', 
            url : "{{route('serialcode.search')}}",
            data:{'action':action,'sorting':sorting,'search':Filter,'perpage':perpage, 'lastItem':lastItem, 'registerstatus':registerstatus },
            xhrFields: {
                responseType: 'blob'
            },
        success: function (response, status, xhr) {

                var filename = "";                   
                var disposition = xhr.getResponseHeader('Content-Disposition');

                 if (disposition) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches !== null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                } 
                var linkelem = document.createElement('a');
                try {
                                           var blob = new Blob([response], { type: 'application/octet-stream' });                        

                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        //   IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) { 
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement("a");

                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.target = "_blank";
                                a.click();
                            }
                        }
                    }   

                } catch (ex) {
                    console.log(ex);
                } 
            }
    });
}


function checkPaginationButtons(){
             pages = $('#totalpages').val();
            current = $('#currentpage').val();

            $('#btnNext').prop('disabled', false);
            $('#btnPrev').prop('disabled', false);

            if(current === pages){
                $('#btnNext').prop('disabled', true);
            } 
            if(current === '1'){
                $('#btnPrev').prop('disabled', true);
            }
            if (pages === '0') {
                $('#btnPrev').prop('disabled', true);
                $('#btnNext').prop('disabled', true);
            }
   }
    
   $('#serialcode_list_status').val(0);
         $(document).on('click', '#serialcoderegistertastus', function(){
            $( "span" ).removeClass("active");
            var status  = $(this).attr('at');
            $(this).addClass( "active" );
            $('#serialcode_list_status').val(status);
            filter_data('registerstatus');
        });

   function deleteItem(id){
       if(id)  
                if(confirm("Are you sure, you want to delete the selected Serialcode ?")){
                    $.ajax({
                        url: "{{ route('serialcode.single-delete') }}",
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+id,
                        success: function (data) {
                            if (data['status']==true) {
                                $(".checkbox:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                location.reload();
                                alert(data['message']);
                            } else {
                               alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                } 
            }

 function print_table(serialcode)
        {  

            $("#serial-list-section .table-row").remove();
             if (serialcode.length >= 1) {            
                $.each(serialcode, function(i,item){
                     var route = "{{route('serialcode.edit','id')}}";
                     route = route.replace('id', item.id);
                   
                  var li = '<li class="table-row"><div><label class="Polaris-Choice" ><span class="Polaris-Choice__Control">';
                  li += '<span class="Polaris-Checkbox">';
                  li += '<input  data-id="'+item.id+'" type="checkbox" class="Polaris-Checkbox__Input checkbox-check" aria-invalid="false" role="checkbox" aria-checked="false" value=""><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">';
                  li += '<path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>';
                  li += '</svg></span></span></span></span></label></div>';
                  li += '<div class="serial-list-text"><h3 class="Polaris-Subheading">';
                  li += '<a href="' + route + '" style="text-decoration : none; color : #0000FF;">'+ item.serial_code + '</a>';
                  li +='</h3><span class="Polaris-TextStyle--variationSubdued date-serial">'+ item.message +'</span><br>';
                  if(item.status == 0){
                   li += '<span class="Polaris-Badge Polaris-Badge--statusSuccess">Active</span>';
                  }else{
                    li += '<span class="Polaris-Badge Polaris-Badge" style="background-color:red; color:white;">InActive</span>';
                  }                                   
                   li +=  '<div class="Polaris-TextStyle--variationSubdued date-serial">From '+ item.start_date+'</div>';
                   li += '</div>';
                   li += '<div style="--top-bar-background:#00848e;--top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">';
                     li += '<button type="button" data-id="' + item.id + '" id="single_delete" onclick="deleteItem('+ item.id +')" class="Polaris-Button Polaris-Button--destructive"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Delete</span></span></button></div>';
                   li +='</li>';
                $('#serial-list-section #show_li_result:last').after(li); 
              });
          }else {
            $("#serial-list-section .table-row").remove();
             var li = '<li style="display:inline-block; text-align: center;width: 100%;" class="table-row">No record found. </li>';
              $('#serial-list-section #show_li_result:last').after(li);  
          } 
              $('#list_select_serial_code_show_code').text('Showing '+ serialcode.length +' Serial code');             
        }


$(document).ready(function(){

  if (typeof $('#texttost').val() !== "undefined" && $('#texttost').val() != ''){
      var msg = $('#texttost').val();
        const toastNotice = actions.Toast.create(crawlapp, {message: msg});
         toastNotice.dispatch(actions.Toast.Action.SHOW);        
       } 
     
 
         $(".multiselect-row").css("display", "none"); 
        $('#check_all,#check_all_label').on('click', function(e) { 
             if($(this).is(':checked',true))  
             {
                $(".checkbox-check").prop('checked', true); 
                $(".check_box_check").prop('checked', true);  
               $(".multiselect-row").css("display", ""); 
             } else { 
                $(".check_box_check").prop('checked', false); 
                $(".checkbox-check").prop('checked',false);
                $(".multiselect-row").css("display", "none");  
                $("#check_all").prop('checked', false); 
             }
             $('#list_select_serial_code').html($('.checkbox-check:checked').length +' Selected');
        });

        $('#serial-list').on('click', '.checkbox-check', function() {
         if($('.checkbox-check:checked').length == $('.checkbox-check').length){
                        $('#check_all').prop('checked',true);
                    }else{
                        $('#check_all').prop('checked',false);
                    }                    
                    $('#list_select_serial_code').html($('.checkbox-check:checked').length +' Selected');
                     sultiselectlabel = ($('.checkbox-check:checked').length >= 1) ?  $(".multiselect-row").css("display", "") :  $(".multiselect-row").css("display", "none");              
                   
        });

        $("#slideToggleaction,#slideToggleactionsort").click(function(){
            $(".custom-dropdwon-ul").slideToggle();
        });       

$('.serailcode_status').on('click', function(e) {
$(".multiselect-row").css("display", "none");
var flag = $(this).val();
var message = '';
if(flag == 0){
   message = 'Active';
}else{
     message = 'InActive';
}  
            var idsArr = [];  
            $(".checkbox-check:checked").each(function() {  
                idsArr.push($(this).attr('data-id'));
            });  
           if(idsArr.length <=0)  
            {  
               alert("Please select atleast one record to "+message+".");  
            }  else {  
                if(confirm("Are you sure, you want to "+message+"."+" the selected Serialcode ?")){  $.ajax({
                         url: "{{ route('serialcode.multiple-status') }}",
                        type: 'get',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data:{'ids':idsArr,'flag': flag },                        
                        success: function (data) {
                            if (data['status']==true) {
                                $(".checkbox:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['message']);
                                location.reload();
                            } else {
                               alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                }  
            }  
        });

     $('.deleted').on('click', function(e) {
            var idsArr = [];  
            $(".checkbox-check:checked").each(function() {  
                idsArr.push($(this).attr('data-id'));
            });  

           if(idsArr.length <=0)  
            {  
               alert("Please select atleast one record to delete.");  
            }  else {  
                if(confirm("Are you sure, you want to delete the selected Serialcode ?")){  
                    var strIds = idsArr.join(","); 
                    $.ajax({
                        url: "{{ route('serialcode.multiple-delete') }}",
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+strIds,
                        success: function (data) {
                            if (data['status']==true) {
                                $(".checkbox:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['message']);
                                location.reload();
                            } else {
                               alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                }  
            }  
        });

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.closest('form').submit();
            }
        });

    checkPaginationButtons();    
    $('#btnPrev').click(function(){
        perpage = parseInt($('#perpage').val());
        lastItem = $('#lastItem').val();
        $('#lastItem').val(parseInt(lastItem)-perpage);
        
        currentpage = $('#currentpage').val();
        $('#currentpage').val(parseInt(currentpage)-1);
        filter_data('paginate');
    });
    
    $('#btnNext').click(function(){
        perpage = parseInt($('#perpage').val());
        lastItem = $('#lastItem').val();
        $('#lastItem').val(parseInt(lastItem)+perpage);        
        currentpage = $('#currentpage').val();
        $('#currentpage').val(parseInt(currentpage)+1);
        
        filter_data('paginate');
    }); 
    
    });    
</script>




