 @extends('layouts.master')

@section('content')
<div class="Polaris-Page">
            <div class="Polaris-Page-Header Polaris-Page-Header--hasNavigation Polaris-Page-Header--hasActionMenu Polaris-Page-Header--mobileView">
                <div class="Polaris-Page-Header__MainContent">
                    <div class="Polaris-Page-Header__Title">
                        <div>
                        <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Add Serial Code</h1>
                        </div>   
                    </div>
                </div>
            </div>
 
 @if ( count( $errors ) > 0 )
   @foreach ($errors->all() as $error)
  
      <div style="color: red;">{{ $error }}</div>
  @endforeach
@endif

 <form id="serial_code_form" method="POST" action="{{ (isset($serialcode)) ? route('serialcode.update', ['serialcode' => $serialcode->id]) : route('serialcode.store') }}" enctype="multipart/form-data" autocomplete="off">
        @csrf 
           @if(!empty($serialcode->product))   
           <input type="hidden" name="id" id="serialcode_id"  value="{{ $serialcode->id }}"/>
               <input id="getcounter" type="hidden" value="{{ count($serialcode->product) }}"/>
           @endif
            <div class="Polaris-Page__Content add-main-row">
                <div class="add-item-main">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                <h2 class="Polaris-Heading">Serial Code</h2>
                                </div>
                                <div class="Polaris-Stack__Item">
                                <div class="Polaris-ButtonGroup">
                                    <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text" onclick="insertAtCaret('serial_code');return false;">Generate Code</span></span></button></div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1">
                            <div class="Polaris-FormLayout serialcode_bulk">
                                <div class="Polaris-FormLayout__Item set_0" >
                                    <div class="label-extra-text">
                                        <div class="Polaris-TextField">
                                            @if(!empty($serialcode->serial_code)) 
                                            <input type="text" id="serial_code"  name="serial_code" class="Polaris-TextField__Input" aria-invalid="false" value="{{ old('serial_code', isset($serialcode->serial_code) ? $serialcode->serial_code : '') }}">
                                                @else
                                                 <textarea id="serial_code"  name="serial_code" class="Polaris-TextField__Input" aria-invalid="false" value="">
                                                </textarea>
                                                @endif
                                              
                                            <div class="Polaris-TextField__Backdrop">
                                            </div>
                                        </div>                              
                                        <p class="mt-1 customer-remove">customer will enter this code to register their product.<div class="serialcodeDiv"></div></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="Polaris-Card" >
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 class="Polaris-Heading">Applies to</h2>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1" id="product_select">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div>
                                        <div class="Polaris-Filters">
                                            <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                                <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                        <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                            <div class="Polaris-Labelled--hidden">
                                                                <div class="Polaris-TextField">
                                                                    <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                        <span class="Polaris-Filters__SearchIcon">
                                                                            <span class="Polaris-Icon">
                                                                                <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    <input id="product_picker" placeholder="Search products" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-Filters-ConnectedFilterControl__MoreFiltersButtonContainer">
                                                        <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                            <div>
                                                                <button type="button" class="Polaris-Button">
                                                                    <span class="Polaris-Button__Content">
                                                                        <span class="Polaris-Button__Text">Browse</span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                         <div class="Polaris-Card__Section" id="product_table">
                              <div class="Polaris-CalloutCard">
                                  <div class="Polaris-CalloutCard__Content">
                                    <div class="Polaris-TextContainer full-table">
                        <table class="table master-datatable table-bordered" id="table-selected">
                                                <thead>
                                                  <tr>
                                                    <th>Title</th>
                                                    <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <input type="hidden" value="" name="fproduct_id[]">
                                                     @php  $count = 0;  @endphp 
                                                    @if(!empty($serialcode->product)) 
                                         @foreach($serialcode->product as $product) 
                                                <tr role="row" class="odd">
                                                    <td>
                                                        <div class="table-float-width">
                                                            <div>
                                                                 <input type="hidden" value="{{ $product->id }}" name="products[{{ $count }}][id]">        
                                                                 <input type="hidden" value="{{ $product->product_id }}" name="products[{{ $count }}][product_id]">
                                                                     <input type="hidden" value="{{ $product->product_title }}" name="products[{{ $count }}][product_title]">
                                                                     <input type="hidden" value="{{ $product->product_id }}" name="product_id[{{ $count }}]">
                                                                      <input type="hidden" name="productids[]" value="{{$product->product_id}}">
                                                                     <input type="hidden" value="{{ $product->image}}" name="products[{{ $count }}][product_image]">
                                                                         <span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium">
                                                                                 <img src="{{ $product->image }}" alt="Black choker necklace" style="vertical-align:middle" class="Polaris-Thumbnail__Image">
                                                                        </span>
                                                            </div>
                                                                <div>{{ $product->product_title }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                            <div class="">
                                                                    <font class="Polaris-Button Polaris-Button--sizeSlim table-action-btn remove" data-image="{{ $product->image }}" data-title="T-Shirt" data-source="{{ $product->product_id }}"><span class="Polaris-Button__Content">
                                                                        <span>Remove </span>
                                                                        </span>
                                                                   </font>
                                                              </div>
                                                      </td>
                                                </tr>
                                            @php  $count++;  @endphp        
                                                @endforeach
                                            @endif                                            
            </tbody>
</table>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
 
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 class="Polaris-Heading">Customer eligbility</h2>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div class="Polaris-Stack Polaris-Stack--vertical">
                                        <div class="Polaris-Stack__Item mt-1">
                                            <div>
                                                <label class="Polaris-Choice" for="everyone"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="everyone" name="customer_eligibility" type="radio" class="Polaris-RadioButton__Input" aria-describedby="disabledHelpText" value="1" {{ isset($serialcode->customer_eligibility)&& $serialcode->customer_eligibility =='1' ? 'checked' : ''}}><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Everyone</span></label>
                                            </div>
                                        </div>
                                        <div class="Polaris-Stack__Item mt-1">
                                            <div>
                                                <label class="Polaris-Choice"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton">
                                                    <input id="show_group" name="customer_eligibility" type="radio" class="Polaris-RadioButton__Input" aria-describedby="optionalHelpText" value="2" {{ isset($serialcode->customer_eligibility)&& $serialcode->customer_eligibility =='2' ? 'checked' : ''}} ><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">specific groups of customers</span></label>
                                            </div>
                                        </div>

                                        <div class="Polaris-Stack__Item mt-1">
                                            <div>
                                                <label class="Polaris-Choice" for="specific_customers"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton">
<input id="specific_customers" name="customer_eligibility" type="radio" class="Polaris-RadioButton__Input" aria-describedby="optionalHelpText" value="3" {{ isset($serialcode->customer_eligibility)&& $serialcode->customer_eligibility =='3' ? 'checked' : ''}}><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">specific customers</span></label>
                                            </div>
                                        </div>
                                        <div class="Polaris-FormLayout__Item" id="list_group_filter" style="display: none">
                                    <div class="Polaris-Filters">
                                        <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                            <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                                <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div class="Polaris-Labelled--hidden">
                                                            <div class="Polaris-TextField">
                                                                <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                    <span class="Polaris-Filters__SearchIcon">
  <span class="Polaris-Icon">
 <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                        </span>
                                                                    </span>
                                                                </div>
 <input id="filter_group" type="text"  placeholder="Search" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Filters-ConnectedFilterControl__MoreFiltersButtonContainer" id="specific_group">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div>
                                                            <button type="button" class="Polaris-Button">
                                                                <span class="Polaris-Button__Content">
                                                                    <span class="Polaris-Button__Text">Browse</span>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="Polaris-Stack Polaris-Stack--vertical">
                                        <div class="Polaris-Stack__Item" id="customer_group_list_select" style="display: none">

                                        <!--  Group counter required -->
                                         @php  $groupcount = 0;   @endphp

                                        @if(!empty($serialcode->CustomerGroup))

                                         @foreach($serialcode->CustomerGroup as $customergroup)
                                          <div class="group-option-list" class="group-option-list group-list" id="{{$customergroup->customer_group_id}}">
                                                <div class="option-name">
                                                {{ $customergroup->group_title }}

                                                 <input type="hidden" name="customer_group_array[]" value="{{$customergroup->customer_group_id}}">

                                                <input type="hidden" name="customer_group_list[{{ $groupcount }}][id]" value="{{$customergroup->id}}">
                                                 <input type="hidden" name="customer_group_list[{{ $groupcount }}][group_id]" value="{{$customergroup->customer_group_id}}">
                                                 <input type="hidden" name="customer_group_list[{{ $groupcount }}][group_name]" value="{{$customergroup->group_title}}">

                                                </div>
                                                <span class="Polaris-Icon" data-id="{{ $customergroup->customer_group_id }}" id="groups_div_remove">
                                                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg>
                                                </span>
                                            </div> 
                                         @php  $groupcount++;  @endphp        
                                                @endforeach                                                  
                                            @endif
                                            <input id="group_count" type="hidden" value="{{ $groupcount }}">     
                                    </div>
                                </div>
                            </div>

                            <!-- ***************Specific customer  ******************** -->
                           <div class="Polaris-FormLayout__Item" id="list_customer_filter" style="display: none">
                                    <div class="Polaris-Filters">
                                        <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                            <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                                <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                       <div class="Polaris-Labelled--hidden">
                                                          <div class="Polaris-TextField">
                                                                <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                    <span class="Polaris-Filters__SearchIcon">
  <span class="Polaris-Icon">
 <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                        </span>
                                                                    </span>
                                                                </div>
 <input id="filter_customer" type="text"  placeholder="Search" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Filters-ConnectedFilterControl__MoreFiltersButtonContainer" id="specific_customer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div>
                                                            <button type="button" class="Polaris-Button">
                                                                <span class="Polaris-Button__Content">
                                                                    <span class="Polaris-Button__Text">Browse</span>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="Polaris-Stack Polaris-Stack--vertical">
                                    <div class="Polaris-Stack__Item" id="customer_list_select" style="display: none">          @php  $customercount = 0;  @endphp           
                                @if(!empty($serialcode->SpecificCustomer))   
                                                                    
                                     
                                         @foreach($serialcode->SpecificCustomer as $specificcustomer)
                                          <div class="group-option-list customer-list" id="{{ $specificcustomer->specific_customer_id }}">
                                                <div class="option-name">
                                                <input type="hidden" name="specific_customer_array[]" value="{{$specificcustomer->specific_customer_id}}">
                                                {{ $specificcustomer->firstname }}  {{ $specificcustomer->lastname }}
                                                <input type="hidden" name="specific_customer[{{ $customercount }}][id]" value="{{ $specificcustomer->id }}">

                                                  <input type="hidden" name="specific_customer[{{ $customercount }}][customer_id]" value="{{ $specificcustomer->specific_customer_id }}">

                                                   <input type="hidden" name="specific_customer[{{ $customercount }}][firstname]" value="{{ $specificcustomer->firstname }}">

                                                    <input type="hidden" name="specific_customer[{{ $customercount }}][lastname]" value="{{ $specificcustomer->lastname }}">
                                                     <input type="hidden" name="specific_customer[{{ $customercount }}][email]" value="{{ $specificcustomer->email }}">
                                                </div>
                                                <span class="Polaris-Icon" data-id="{{ $specificcustomer->specific_customer_id }}" id="customer_div_remove">
                                                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg>
                                                </span>
                                            </div> 
                                         @php  $customercount++;  @endphp        
                                                @endforeach
                                            @endif
                                            <input id="customer_count" type="hidden" value="{{ $customercount }}">

                                    </div>
                                </div>
                            </div>
    
                            <!-- *******End specific customer*************** -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
 
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 class="Polaris-Heading">Usage limits</h2>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div class="Polaris-Stack Polaris-Stack--vertical">
                                        <div class="Polaris-Stack__Item mt-1">
                                            <div>
                                                <label class="Polaris-Choice" for="limit_number_of_time"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox">
                                           
                                            <input id="limit_number_of_time" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value="1" name="limit_number_of_time" {{ isset($serialcode->limit_number_of_time)&& $serialcode->limit_number_of_time =='1' ? 'checked' : ''}}>
                                              <span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></span></span></span><span class="Polaris-Choice__Label">Limit number of times this serial code can be used in total</span></label>
                                            <div class="Polaris-Choice__Descriptions" id="limit_number_user">
                                                <div class="Polaris-TextField Polaris-TextField--hasValue w-50">
                                                    <input  name="limit_number_user" class="Polaris-TextField__Input" value="{{ old('limit_number_user', isset($serialcode->limit_number_user) ? $serialcode->limit_number_user : '') }}">
                                                  <div class="Polaris-TextField__Backdrop"></div>
                                                </div>
                                            </div>
                                        </div>
<?php //exit; ?>
                                        <div class="Polaris-Stack__Item mt-1">
                                            <div>
                                                <label class="Polaris-Choice" for="limit_one_per_user"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox">
                                                    <input id="limit_one_per_user" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" name="limit_one_per_user" value="1" {{ isset($serialcode->limit_one_per_user)&& $serialcode->limit_one_per_user =='1' ? 'checked' : ''}}>
                                                    <span class="Polaris-Checkbox__Backdrop"></span>
                                                    <span class="Polaris-Checkbox__Icon">
                                                        <span class="Polaris-Icon">
                                                            <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span><span class="Polaris-Choice__Label">Limit to one use per customer</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                        </div>
                    </div>
               </div>
           </div>  
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 class="Polaris-Heading">Active dates</h2>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div class="active-dates-row">
                                        <div class="w-50">
                                            <div class="Polaris-Labelled__LabelWrapper">
                                                <div class="Polaris-Label"><label id="TextField1Label" for="TextField1" class="Polaris-Label__Text">Start date  </label></div>
                                            </div>
                                            <div class="Polaris-TextField">
                                                <div class="Polaris-TextField__Prefix" id="TextField1Prefix"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                                                <svg id="Layer_1" class="Polaris-Icon__Svg" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path class="cls-1" d="M64,11a6,6,0,0,0-6-6H51.7V2a2,2,0,0,0-4,0V4.9H16.3V2a2,2,0,0,0-4,0V4.9H6a6,6,0,0,0-6,6v9.2a.37.37,0,0,0,.1.3.76.76,0,0,0-.1.4H0V58a6,6,0,0,0,6,6H58a6,6,0,0,0,6-6V20.8h0a.37.37,0,0,0-.1-.3c0-.1.1-.2.1-.3ZM60,59.1a.79.79,0,0,1-.8.8H4.8c-.4.1-.8-.3-.8-.8V22.8H60Zm0-40.3H4v-9A.79.79,0,0,1,4.8,9H59.1a.82.82,0,0,1,.9.8Z"/><circle class="cls-1" cx="52.2" cy="31.9" r="3.7"/><circle class="cls-1" cx="32.2" cy="31.9" r="3.7"/><circle class="cls-1" cx="11.8" cy="31.9" r="3.7"/><circle class="cls-1" cx="32.2" cy="44.8" r="3.7"/><circle class="cls-1" cx="11.8" cy="44.8" r="3.7"/></svg></span></div>

                <input id="start_date" name="start_date"  placeholder="Enter Date" value="{{ old('start_date', isset($serialcode->start_date) ? $serialcode->start_date : '') }}" autocomplete="off" class="Polaris-TextField__Input" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" aria-autocomplete="list" aria-controls="Popover1" value="" tabindex="0" aria-owns="Popover1" aria-haspopup="true" aria-expanded="false">
                                                <div class="Polaris-TextField__Backdrop"></div>
                                            </div>       
                                        </div>                              
                                    </div>                                                                         
                                </div>
                            </div>
                        </div>
                    </div>              
        
        </div>
                <div class="add-item-sub">
                    <div class="Polaris-Card summary-card">
                        <div class="Polaris-Card__Header">
                            <h2 class="Polaris-Heading" >Summary</h2>
                        </div>
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-Card__SectionHeader mb-0">
                                <h3 aria-label="Reports" class="Polaris-Heading" id="summary">{{ isset($serialcode->serial_code) ? $serialcode->serial_code : '' }}
                                </h3>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-Card__SectionHeader">
                                <h3 aria-label="Summary" class="Polaris-Subheading">Performance</h3>
                            </div>
                            <p>Serial is not active yet.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="Polaris-PageActions">
                <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing">
                    <div class="Polaris-Stack__Item">
                    <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                             <a href="{{route('serialcode.index')}}" style="color:black;">
                            <button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button>
                        </a></div>
                    </div>
                    </div>
                    <div class="Polaris-Stack__Item"><button type="submit" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save Serial code</span></span></button></div>
                </div>
            </div>
   </form>
        </div>
        <div class="Polaris-Modal-Dialog__Container popup-customer-group" style="display: none;">
            <div>       
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header15" tabindex="-1">
                <div class="Polaris-Modal-Header">
                <div id="modal-header15" class="Polaris-Modal-Header__Title">
                    <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Add customer groups</h2>
                </div><button class="Polaris-Modal-CloseButton" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                        <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg></span></button>
                </div>
              <form name="customer_broup" id="find-table">
                <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                    <div class="Polaris-Card__Section Polaris-Page-Header--separator">
                        <div class="Polaris-FormLayout">
                            <div class="Polaris-FormLayout__Item">
                                <div class="Polaris-Filters">
                                    <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                        <div class="Polaris-Filters-ConnectedFilterControl">
                                            <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                    <div class="Polaris-Labelled--hidden">
                                                        <div class="Polaris-TextField">
                                                            <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                <span class="Polaris-Filters__SearchIcon">
                                                                    <span class="Polaris-Icon">
                                                                        <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <input id="customer_group_filter" placeholder="Search" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" value="{{ request('search') }}" 
                                                            aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-groups-div">
                        <ul id="customer_group_list">
                            <li style="display:none;"></li>
                            <li class="table-row">
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                 <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                    <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                        <div class="Polaris-ButtonGroup">
                                <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button" id="close_group_popup"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button></div>
                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary" id="group_form"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add</span></span></button></div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                </form> 
            </div>
            
            </div>
        </div>

        <!-- *****Specific customer popup************* -->
         <div class="Polaris-Modal-Dialog__Container popup-customer" style="display: none;">
            <div>
       
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header15" tabindex="-1">
                <div class="Polaris-Modal-Header">
                <div id="modal-header15" class="Polaris-Modal-Header__Title">
                    <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Add customer</h2>
                </div><button class="Polaris-Modal-CloseButton" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                        <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg></span></button>
                </div>
              <form name="customer_popup" id="customer-table">
                <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                    <div class="Polaris-Card__Section Polaris-Page-Header--separator">
                        <div class="Polaris-FormLayout">
                            <div class="Polaris-FormLayout__Item">
                                <div class="Polaris-Filters">
                                    <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                        <div class="Polaris-Filters-ConnectedFilterControl">
                                            <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                    <div class="Polaris-Labelled--hidden">
                                                        <div class="Polaris-TextField">
                                                            <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                <span class="Polaris-Filters__SearchIcon">
                                                                    <span class="Polaris-Icon">
                                                                        <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <input id="customer_filter" placeholder="Search" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" value="{{ request('search') }}"
                                                            aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-groups-div">
                        <ul id="customer_list">
                            <li style="display:none;"></li>
                            <li class="customer-row">
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                 <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                    <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                        <div class="Polaris-ButtonGroup">
                                <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button" id="close_customer_popup"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button></div>
                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary" id="customer_form"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add</span></span></button></div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                </form> 
            </div>
            
            </div>
        </div>
     
        <!-- ******End specif group popup*********** -->
      <div class="Polaris-Backdrop" style="display: none"></div>  

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>


<script type="text/javascript">
var template = $('.serialcode_bulk .set_0:first').clone();
const productPicker = actions.ResourcePicker.create(crawlapp, {
  resourceType: actions.ResourcePicker.ResourceType.Product,
    options: {
        selectMultiple: true,
        showHidden: false,
        showVariants: false,
    }
});

var remove_product_data = '';
var groups_array = [];
var customer_array = [];
var ProductArray = [];

var sectionsCount = 1;
var crawlapps_serial_app = {
    init: function(){
        $(".popup-customer-group").css("display", "none");
        $(".popup-customer").css("display", "none");
        $(".Polaris-Backdrop").css("display", "none");
        this.common();
        this.existingProductTable();
        this.filter_customer_group();
        this.customerEligibility();
        this.SecificCustomer();
        this.usageLimit();
        this.addserialcode();
        this.removeserialcode();
    },
    addserialcode: function(){

        $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: '-3d'
        });        
       
       if (typeof $("#start_date").val() == "undefined" || $("#start_date").val() == '') {
             $("#start_date").datepicker("setDate", new Date());
        }  

        $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: '-3d'
        });

        $('#start_time').bootstrapMaterialDatePicker({ date: false,time: true,format: 'HH:mm',timeFormat: 'HH:mm:ss',switchOnClick: true });
    $('.addBankDetails').on('click',function(){
           var section = template.clone().find(':input').each(function(){
               var newId = this.id + sectionsCount;
               var field_name = this.dataset.field_name;
               var name = "serial_code[]";
               $(this).prev().attr('for', newId);
               this.name = name;
               this.value = "";
               if(this.id == "bank_id"){
                   this.value = "";
               }else{
                   this.id = newId;
               }
           }).end()
           //inject new section
           .appendTo('.serialcode_bulk');
           $(section[0]).attr("id","set_0"+sectionsCount);
           $("#set_0"+sectionsCount+" .ifsc_error0").attr("id","ifsc_error0"+sectionsCount);
           $("#set_0"+sectionsCount+" .serialcodeDiv").html('<div align="right"> <a href="#" class="serialcoderemove">Remove</a></div>');
           sectionsCount++;
           return false;
       });
   },


   removeserialcode : function(){
       $('.serialcode_bulk').on('click', '.serialcoderemove', function() {
           $(this).parent().parent().parent().empty();
           return false;
           return false;
       });
   },
   usageLimit: function(){
      
        $('#limit_number_user').hide();
        $('#limit_number_of_time').on("click",function() {     
            var is_check_number =  $("#limit_number_of_time").is(':checked');   
                if(!is_check_number){
                    $('#limit_number_user').hide();
                }else{
                   $('#limit_number_user').show();
                }            
         });

        var serialcodeid = $('#serialcode_id').val();
      if (typeof serialcodeid === "undefined" || serialcodeid == '') {
         $( "#everyone" ).prop( "checked", true );
      } 
    var limit_number =  $("#limit_number_of_time").is(':checked');
    var serialcodecheck = $('#serialcode_id').val();
  
    if (typeof serialcodecheck === "undefined" || serialcodecheck == '') {
            $( "#limit_one_per_user" ).prop( "checked", true );
      } 
        if(limit_number){
             $('#limit_number_user').show();
           }
        $('#limit_one_per_user').on("click",function() {        
        var is_check_number =  $("#limit_number_of_time").is(':checked');   
           if(!is_check_number){
             $('#limit_number_user').hide();
           }else{
            $('#limit_number_user').show();
           }
        });      
    },    
      customerEligibility: function(){

      var serialcodeid = $('#serialcode_id').val();
      if (typeof serialcodeid === "undefined" || serialcodeid == '') {
         $( "#everyone" ).prop( "checked", true );
      }     
        var group =  $("#show_group").is(':checked');
        if(group){
            $("#list_group_filter").css("display", "");
            $("#customer_group_list_select").css("display", "");
        }

        var specificcust =  $("#specific_customers").is(':checked');
        if(specificcust){
            $("#list_customer_filter").css("display", "");
            $("#customer_list_select").css("display", "");
        }
        $("#show_group").click(function () { 
            $("#list_group_filter").css("display", "");
            $("#customer_group_list_select").css("display", "");
            $("#list_customer_filter").css("display", "none");
            $("#customer_list_select").css("display", "none");
          });
         $("#everyone").click(function () { 
            $("#list_group_filter").css("display", "none");
            $("#customer_group_list_select").css("display", "none");
            $("#list_customer_filter").css("display", "none");
            $("#customer_list_select").css("display", "none");
          });        

         $("#specific_customers").click(function () { 
            $("#list_group_filter").css("display", "none");
            $("#customer_group_list_select").css("display", "none");
            $("#list_customer_filter").css("display", "");
            $("#customer_list_select").css("display", "");
          });
     },

    filter_customer_group: function(){
        $("#filter_group").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#customer_group_list_select *").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
         
        $('#group_form').on("click",function() {
           var removedivRrrayGroup = [];
            $("#customer_group_list_select").css("display", "");
            var countproduct = $('#group_count').val();
            var searchIDs = $("#find-table input:checkbox:checked").map(function(){
                 var checkval = $(this).val();
                 var slpitval =  checkval.split('^');
                   removedivRrrayGroup.push(slpitval[0]);
                if (jQuery.inArray(slpitval[0], groups_array)=='-1') {
                     Interest(slpitval[0]);
                    var li =  '<div class="group-option-list group-list" id="'+slpitval[0]+'"><div class="option-name">';
                     li += ''+slpitval[1]+' </div>';
                     li += '<input type="hidden" value="'+slpitval[0]+'" name="customer_group_list['+ countproduct +'][group_id]">'; 
                    li += '<input type="hidden" value="'+slpitval[1]+'" name="customer_group_list['+ countproduct +'][group_name]">'; 
                      li += '<input type="hidden" class="customer_group_dublicate" value="'+slpitval[0]+'" name="customer_group['+ countproduct +']">'; 
                     li += '<span class="Polaris-Icon" id="groups_div_remove" data-id="'+slpitval[0]+'"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span>';
                     li += ' </div></div>'; 
                      $("#customer_group_list_select").append(li);
                }       
                      countproduct++;
                      return slpitval[0];
                        });

             jQuery.grep(groups_array, function(el) {
                    if (jQuery.inArray(el, removedivRrrayGroup)=='-1') {
                       $("#" + el).remove(); 
                    }
             }); 

             jQuery.grep(groups_array, function(el) {
                    if (jQuery.inArray(el, removedivRrrayGroup)=='-1') {
                       groups_array.splice($.inArray(el, groups_array),1); 
                    }
             });       

            if(searchIDs.length == '0'){ 
                $('.group-list').remove(); 
            }    
            $(".popup-customer-group").css("display", "none");
             $(".Polaris-Backdrop").css("display", "none");
     });

        function  Interest(groupid){
         groups_array.push(groupid);
       }
       var GroupArray = $("input[name='customer_group_array[]']")
              .map(function(){return $(this).val();}).get();
         if(GroupArray){
             $.each(GroupArray, function(i,item){
                 groups_array.push(item);
             });
         }

        $("#specific_group").click(function () {  
             commoncustomerGroup('onload');
         });

        function commoncustomerGroup(action){
            var Filter = $('#customer_group_filter').val();
                $.ajax({
                    type : 'get', 
                    url : "{{route('customers.groups')}}",
                    data:{'action':action,'filter':Filter},
                    success:function(data){
                        customergroupfilter = JSON.parse(data);
                        print_table(customergroupfilter); 
                    }
                });   
                    $(".popup-customer-group").css("display", "");
                    $(".Polaris-Backdrop").css("display", ""); 
        }


        $("#customer_group_filter").on("keyup", function() {
            commoncustomerGroup('search');            
        });
         
         function print_table(data)
        {         
          if (data.length >= 1) {
            var counter = 0;
              $("#customer_group_list .table-row").remove();
            $.each(data, function(i,item){
                var slpitval =  item.group_id.split('/');
                var group_id = slpitval[4];
                 var checked = '';
                if (jQuery.inArray(group_id, groups_array) !='-1') {
                     checked = 'checked';
                }                                                                   
                var li =  '<li class="table-row">';
                   li += '<label class="Polaris-Choice">';
                   li += '<span class="Polaris-Choice__Control">';
                   li += '<span class="Polaris-Checkbox">';
                   li += '<input id="group_'+group_id+'" type="checkbox" class="Polaris-Checkbox__Input groups_value checkbox-check" aria-invalid="false" role="checkbox" aria-checked="false" name="group_id["'+ counter +'"]" value="'+group_id+'^'+item.group_name+'" data-id="'+item.group_id+'"'+ checked +'><input  type="hidden" class="Polaris-Checkbox__Input groups_name" aria-invalid="false" name="groups_name["'+ counter +'"]" value="'+item.group_name+'" >';
                   li += '<span class="Polaris-Checkbox__Backdrop"></span>';
                   li += '<span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon">';
                   li += '<svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">';
                   li += '<path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>';
                   li += '</svg></span></span></span></span><span class="Polaris-Choice__Label">'+ item.group_name +'</span></label>';
                   li += '</li>';
                   $( "#customer_group_list li:last" ).after(li);
             counter++;
        });
        }else{
            $("#customer_group_list .table-row").remove();
            var li = '<li class="table-row">';
                li += ' No record found.';
                li += '</tr>';       
           $( "#customer_group_list li:last" ).after(li);
   }
        }

        $(".Polaris-Modal-CloseButton").click(function () {
            $(".popup-customer-group").css("display", "none");
            $(".Polaris-Backdrop").css("display", "none"); 
         });
        $("#close_group_popup").click(function () {
              $(".popup-customer-group").css("display", "none");
              $(".Polaris-Backdrop").css("display", "none");
         });         
          
       $('#customer_group_list_select').on('click', '#groups_div_remove', function() {
            $("#" + $(this).attr("data-id")).remove(); 
            $("#group_" + $(this).attr("data-id")).prop("checked", false);
            groups_array.splice($.inArray($(this).attr("data-id"), groups_array),1);
       });

    },

    SecificCustomer: function(){
        $("#filter_customer").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#customer_list_select *").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
         
        $('#customer_form').on("click",function() {
            var removedivarraydiv = [];
            $("#customer_list_select").css("display", "");
            var countcustomer = $('#customer_count').val();
            var searchIDs = $("#customer-table input:checkbox:checked").map(function(){
                 var checkval = $(this).val();
                 var slpitval =  checkval.split('^');
                 removedivarraydiv.push(slpitval[0]);
                if (jQuery.inArray(slpitval[0], customer_array)=='-1') {
                     customerInterest(slpitval[0]);
                    var li =  '<div class="group-option-list customer-list" id="'+slpitval[0]+'"><div class="option-name">';
                     li += ''+slpitval[1]+ " "+slpitval[2] +' </div>';
                     li += '<input type="hidden" class="customer_customer_dublicate" value="'+slpitval[0]+'" name="customer['+ countcustomer +']">'; 
                     li += '<input type="hidden" value="'+slpitval[0]+'" name="specific_customer['+ countcustomer +'][customer_id]">'; 
                     li += '<input type="hidden" value="'+slpitval[1]+'" name="specific_customer['+ countcustomer +'][firstname]">';
                     li += '<input type="hidden" value="'+slpitval[2]+'" name="specific_customer['+ countcustomer +'][lastname]">';
                     li += '<input type="hidden" value="'+slpitval[3]+'" name="specific_customer['+ countcustomer +'][email]">';

                     li += '<span class="Polaris-Icon" id="customer_div_remove" data-id="'+slpitval[0]+'"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span>';
                     li += ' </div></div>'; 
                      $("#customer_list_select").append(li);
                }     
                      countcustomer++;
                      return slpitval[0];
                    });

             jQuery.grep(customer_array, function(el) {
                if (jQuery.inArray(el, removedivarraydiv)=='-1') {
                    $("#" + el).remove(); 
                    customer_array.splice($.inArray(el, customer_array),1);
                    }
             });
            
            if(searchIDs.length == '0'){ 
                $('.customer-list').remove(); 
            }   
            $(".popup-customer").css("display", "none");           
           $(".Polaris-Backdrop").css("display", "none"); 
     });

        function  customerInterest(groupid){
         customer_array.push(groupid);
        // $(".customer_group_dublicate").each(function () {
        //        groups_array.push($(this).val());   //  });
       }

       var CustomerArray = $("input[name='specific_customer_array[]']") 
              .map(function(){return $(this).val();}).get();
         if(CustomerArray){
             $.each(CustomerArray, function(i,item){
                 customer_array.push(item);
             });
         }

           $("#specific_customer").click(function () {  
              commonspecificcustomer('onload');
         });

        function commonspecificcustomer(action){
            var Filter = $('#customer_filter').val();
                  $.ajax({
                            type : 'get', 
                            url : "{{route('customers.specific')}}",
                            data:{'action':action ,'filter':Filter},
                            success:function(data){
                                 data = JSON.parse(data);
                                 customer_table(data); 
                             }
                });  
                        $(".popup-customer").css("display", "");      
                        $(".Polaris-Backdrop").css("display", "");
        }

        $("#customer_filter").on("keyup", function() {
            commonspecificcustomer('search');            
        });        

         function customer_table(data)
        {         
          if (data.length >= 1) {
            var counter = 0;
             $("#customer_list .customer-row").remove();
            $.each(data, function(i,item){

                var slpitval =  item.customer_id.split('/');
                var customer_id = slpitval[4];
                var checked = '';
                if (jQuery.inArray(customer_id, customer_array) !='-1') {
                     checked = 'checked';
                } 
                var li =  '<li class="customer-row">';
                   li += '<label class="Polaris-Choice">';
                   li += '<span class="Polaris-Choice__Control">';
                   li += '<span class="Polaris-Checkbox">';
                   li += '<input id="customer_'+customer_id+'" type="checkbox" class="Polaris-Checkbox__Input groups_value checkbox-check" aria-invalid="false" role="checkbox" aria-checked="false" name="customer["'+ counter +'"]" value="'+customer_id+'^'+item.firstname+'^'+ item.lastname+'^'+item.email+'" data-id="'+customer_id+'"'+checked+'><input  type="hidden" class="Polaris-Checkbox__Input customer_name" aria-invalid="false" name="customer_name["'+ counter +'"]" value="'+item.firstname+' '+ item.lastname +'">';
                   li += '<span class="Polaris-Checkbox__Backdrop"></span>';
                   li += '<span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon">';
                   li += '<svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">';
                   li += '<path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>';
                   li += '</svg></span></span></span></span><span class="Polaris-Choice__Label">'+ item.firstname +' '+item.lastname +'</span></label>';
                   li += '</li>';
                   $( "#customer_list li:last" ).after(li);
             counter++;
        });
        }else{
            $("#customer_list .customer-row").remove();
            var li = '<li class="customer-row">';
                li += ' No record found.';
                li += '</tr>';       
           $("#customer_list li:last").after(li);
   }
}   

        $(".Polaris-Modal-CloseButton").click(function () {
             $(".popup-customer").css("display", "none");
             $(".Polaris-Backdrop").css("display", "none");
         });
        $("#close_customer_popup").click(function () {
             $(".popup-customer").css("display", "none");
             $(".Polaris-Backdrop").css("display", "none"); 
         });         
          
       $('#customer_list_select').on('click', '#customer_div_remove', function() {
            $("#" + $(this).attr("data-id")).remove(); 
            $("#customer_" + $(this).attr("data-id")).prop("checked", false);
            customer_array.splice($.inArray($(this).attr("data-id"), customer_array),1);
       });
    },
    common: function(){
        /* GENERATE CODE */
        $(document).on("click","#generateRandomString",function(){

            var length = 13, serialcode = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";   
            for (var i = 0; i < length; i++){
                serialcode += possible.charAt(Math.floor(Math.random() * possible.length)).toUpperCase(); 
            }
             var code = serialcode.slice(0, 4);
             var code1 = serialcode.slice(4, 9);
             var code2 = serialcode.slice(9, 13);
            $('#summary').text(code+'-'+code1+'-'+code2);
            $('#serial_code').val(code+'-'+code1+'-'+code2);


        });         
        var Product_array_id = $("input[name='productids[]']") 
              .map(function(){return $(this).val();}).get();
         if(Product_array_id){
            $('#product_table').show();
             $.each(Product_array_id, function(i,item){
                 ProductArray.push(item);
             });
         }else{
            $('#product_table').hide();
         }

        /* GET SELECTD PRODUCT*/
        $("#product_select").click(function () {
          productPicker.dispatch(actions.ResourcePicker.Action.OPEN);
          var loopcount = 0;
          productPicker.subscribe(actions.ResourcePicker.Action.SELECT, ({selection}) => {
           if(loopcount == 0){
                var count = 0; 
                if($("#getcounter").val() >= 0){
                    count = $("#getcounter").val();   
                }
                 $.each(selection, function(i,item){
                 var slpitval =  item.id.split('/');
                 var p_id = slpitval[4];
              
                  if (jQuery.inArray(p_id, ProductArray)=='-1') {
                     productpusharray(p_id);
                        var title = item.title;
                        var image = "";
                        if(item.images.length > 0){
                            image = item.images[0].originalSrc;
                        }else{
                            image = "{{asset('image/noimage_product.svg')}}";
                        }
                        var title_html = crawlapps_serial_app.titleHtml(title,image,p_id,count); 
                        var remove = '<div class=""><font class="Polaris-Button Polaris-Button--sizeSlim table-action-btn remove" data-image="'+image+'" data-title="'+title+'"  data-source="'+p_id+'"><span class="Polaris-Button__Content"><span>Remove</span></span></font></div>';
                        remove_product_data.row.add([
                            title_html, 
                            remove,
                         ]).draw( false );
                     count++; 
             }
                });
           }
                 loopcount++;
                });  
        }); 

        function  productpusharray(product_id){
            ProductArray.push(product_id);  
            $('#product_table').show();
       } 
       
    },
    titleHtml: function(title,image,p_id,count){
    var html = '<div class="table-float-width">';
        html +=  '<div>';
        html +=  '<input type="hidden" value="'+ p_id +'" name="products['+ count +'][product_id]">';
        html +=  '<input type="hidden" value="'+ title +'" name="products['+ count +'][product_title]">';
        html +=  '<input type="hidden" value="'+ p_id +'" id="product_list" name="product_id['+ count +']">';
        html +=  '<input type="hidden" value="'+ image +'" name="products['+ count +'][product_image]">';
        html +=  '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="'+image+'" alt="Black choker necklace" style="vertical-align:middle" class="Polaris-Thumbnail__Image"></span>';
        html +=  '</div>';
        html +=  '<div>'+title+'</div>';
        html +=  '</div>';
        return html;
  },
    existingProductTable: function(){
    remove_product_data = $('#table-selected').DataTable({
            language: {
                "zeroRecords": '<div class="ui-card__section"><div class="ui-type-container"><div class="empty-search-results" illustration="next-products"><svg class="next-icon next-icon--color-sky-darker next-icon--size-80 empty-search-results__illustration"  style="width:80px;height:80px;"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search" style="fill: #c3cfd8;"><svg id="search"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z"></path></svg></svg></use> </svg><h2 class="empty-search-results__title">Could not find any records</h2><p class="empty-search-results__message">Try changing the search term</p></div></div></div>',
                "sEmptyTable": "<span class='no-records'>No records available<span>",
                "sLoadingRecords": "Please wait - loading...",
                "oPaginate": {
                  "sNext": '<nav class="Polaris-Pagination" aria-label="Pagination"><button class="Polaris-Pagination__Button" type="button" aria-label="Next"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></button>',
                  "sPrevious": '<button class="Polaris-Pagination__Button" type="button" aria-label="Previous"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></button>'
                },
                "sSearch": ""
            },
            "lengthChange": false,
            "pageLength": 10,
            "orderable": true,
            "bSort": false,
            "bInfo" : false,
            "fnDrawCallback": function(oSettings) {
                if($("#table-selected_paginate span a").length >= 2){
                    $("#table-selected_paginate > span").hide();
                }else if($("#table-selected_paginate span a").length <= 1){
                    $("#table-selected_paginate").hide();
                }
            }
        });


         $(document).on("click",".table-action-btn.remove",function(){
            var p_id = this.dataset.source;
            ProductArray.splice($.inArray(p_id, ProductArray),1);
            remove_product_data.row( $(this).parents('tr') ).remove().draw( false );
        });
  },
};

$(document).ready(function(){
  crawlapps_serial_app.init();

  $(window).keydown(function(event){
      if(event.target.tagName != 'TEXTAREA') {
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      }
  });
});


$("#serial_code").keyup(function(){
    summycode();
});

 function insertAtCaret(areaId) {
  var txtarea = document.getElementById(areaId);
  var length = 13, serialcode = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";   
            for (var i = 0; i < length; i++){
                serialcode += possible.charAt(Math.floor(Math.random() * possible.length)).toUpperCase(); 
            }
             var code = serialcode.slice(0, 4);
             var code1 = serialcode.slice(4, 9);
             var code2 = serialcode.slice(9, 13);
            // $('#summary').text(code+'-'+code1+'-'+code2);
            // $('#serial_code').val(code+'-'+code1+'-'+code2);

            text = code+'-'+code1+'-'+code2; 

            var summary = $('#summary').text();

            var nameArr = summary.split(','); 
            if(nameArr.length == 1){
                $('#summary').append(text+',');
            }else{
                $('#summary').append(','+text);
            }
 
  var scrollPos = txtarea.scrollTop;
  var strPos = 0;
  var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
    "ff" : (document.selection ? "ie" : false));
  if (br == "ie") {
    txtarea.focus();
    var range = document.selection.createRange();
    range.moveStart('character', -txtarea.value.length);
    strPos = range.text.length;
  } else if (br == "ff") {
    strPos = txtarea.selectionStart;
  }

  var front = (txtarea.value).substring(0, strPos);
  var back = (txtarea.value).substring(strPos, txtarea.value.length);
  txtarea.value = front + text + back;
  strPos = strPos + text.length;
  if (br == "ie") {
    txtarea.focus();
    var ieRange = document.selection.createRange();
    ieRange.moveStart('character', -txtarea.value.length);
    ieRange.moveStart('character', strPos);
    ieRange.moveEnd('character', 0);
    ieRange.select();
  } else if (br == "ff") {
    txtarea.selectionStart = strPos;
    txtarea.selectionEnd = strPos;
    txtarea.focus();
  }  
  txtarea.scrollTop = scrollPos;
  summycode();
}

function summycode(){
    var summatyCode = $('#serial_code').val().split("\n");
   $('#summary').html('');
   if (summatyCode.length >= 1) {
      $.each(summatyCode, function (i, item) {
         if(item != ''){
            $('#summary').append('<p>'+ item +'</p>');
         }        
       })
     }
}

</script>
@endsection