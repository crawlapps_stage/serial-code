@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
@endsection

@section('content')

<main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
    <div class="Polaris-Frame__Content  Polaris-Page">
        <div class="breadcrumb">

            <div class="">
                <div
                    class="Polaris-Page-Header breadcrumb_header Polaris-Page-Header__Header--hasPagination Polaris-Page-Header__Header--hasBreadcrumbs Polaris-Page-Header__Header--hasRollup Polaris-Page-Header__Header--hasSecondaryActions">
                    <div class="Polaris-Page-Header__Navigation">
                        <nav role="navigation">
                            <a class="Polaris-Breadcrumbs__Breadcrumb"
                               href="{{route('serialcode.index')}}" data-polaris-unstyled="true">
                                <span
                                    class="Polaris-Breadcrumbs__Icon">
                                    <span
                                        class="Polaris-Icon">
                                        <svg viewBox="0 0 20 20"
                                             class="Polaris-Icon__Svg" focusable="false"
                                             aria-hidden="true">
                                        <path
                                            d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16"
                                            fill-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </span>
                                <span
                                    class="Polaris-Breadcrumbs__Content">Dashboard</span>
                            </a>
                        </nav>
                        <button type="button"
                                class="Polaris-Button Polaris-Button--primary"><span
                                class="Polaris-Button__Content"><a
                                    href="{{route('settings.create')}}"
                                    style="color:white;"><span
                                        class="Polaris-Button__Text">Add Settings</span></a></span></button>
                       
                    </div>
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">
                        Settings
                    </h1>
                </div>
            </div>
        </div>

        <div class="influencer_datalist">
            <div class=" Polaris-Page__Content">
                <div class="influencer_search">
                    <div style="height: 225px;">
                        <div role="combobox"  style="outline: none !important;" aria-expanded="false" aria-owns="ComboBox5" aria-controls="ComboBox5" aria-haspopup="true" tabindex="0">
                            <div class="datatable" style="outline: none !important;">
                                <div class="search">
                                    <div class="Polaris-TextField">
                                        <div class="Polaris-TextField__Prefix"
                                             id="TextField5Prefix">
                                            <span
                                                class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                                                <svg
                                                    viewBox="0 0 20 20"
                                                    class="Polaris-Icon__Svg" focusable="false"
                                                    aria-hidden="true">
                                                <path
                                                    d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414"
                                                    fill-rule="evenodd"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <input id="TextField5" placeholder="Search"
                                               value="{{ request('search') }}" onkeyup="filter_data('search')"
                                               autocomplete="off" class="Polaris-TextField__Input"
                                               aria-label=""
                                               aria-labelledby="TextField5Label TextField5Prefix"
                                               aria-invalid="false" aria-autocomplete="list"
                                               aria-controls="Popover5" tabindex="0"
                                               aria-owns="Popover5" aria-haspopup="true"
                                               aria-expanded="false">
                                        <div class="Polaris-TextField__Backdrop"></div>
                                    </div>
                                </div>
                                <div class="influencer_datatable">
                                    <div class="Polaris-Page__Content">
                                        <div style="overflow: auto;">
                                            <div class="">
                                                <div class="Polaris-DataTable">
                                                    <div>
                                                        <table id="tablesettings" class="Polaris-DataTable__Table"
                                                               style="text-align:left;">
                                                             <thead>
                                                            <tr> 
                                                                 <th data-polaris-header-cell="true"
                                                                        class="Polaris-DataTable__Cell--header "
                                                                        scope="col"
                                                                        aria-sort="none"
                                                                        style="height: 48px;">
                                                                        <span
                                                                            class="Polaris-TextStyle--variationStrong">Setting
                                                                            name</span>
                                                                    </th>
                                                                    <th data-polaris-header-cell="true"
                                                                        class="Polaris-DataTable__Cell--header "
                                                                        scope="col"
                                                                        aria-sort="none"
                                                                        style="height: 48px;">
                                                                        <span
                                                                            class="Polaris-TextStyle--variationStrong">Setting
                                                                            value</span>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if (count($getsettings) >= 1)
                                                                @foreach($getsettings as $key=>$settings)
                                                                <tr
                                                                    class="Polaris-DataTable__TableRow table-row">
                                                                    <td class=""
                                                                        style="height: 55px;">
                                                                        <a href="{{route('settings.edit',$settings->id)}}" style="text-decoration : none; color : #0000FF;">{{ $settings->name }}</a>
                                                                    </td>
                                                                    <td class=""
                                                                        style="height: 55px;">
                                                                        {{ $settings->value }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                                @else
                                                                <tr
                                                                    class="Polaris-DataTable__TableRow table-row">
                                                                    <td class="text-center"
                                                                        style="height: 55px;" colspan="4" align="center">
                                                                        No record found.
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div id="pagination_fields">
                                        <input type="hidden" name="perpage" id="perpage" value="{{ $perpage }}" />
                                        <input type="hidden" name="lastItem" id="lastItem" value="{{ $offset }}" />
                                        <input type="hidden" name="totalpages" id="totalpages" value="{{ $totalpages }}" />
                                        <input type="hidden" name="currentpage" id="currentpage" value="{{ $currentpage }}" />
                                    </div>
                                     @if(session()->get('success'))
                                            <div class="tost_msg">
                                                <input type="hidden" name="texttost" id="texttost" value="{{ session()->get('success') }}" />
                                            </div>
                                      @endif
                                    

                                    <div class="influencer_arrow">
                                        <nav class="Polaris-Pagination" aria-label="Pagination">
                                            <button type="button"
                                                    id="btnPrev"
                                                    class="Polaris-Pagination__Button"
                                                    aria-label="Previous">
                                                <span
                                                    class="Polaris-Icon">
                                                    <svg
                                                        viewBox="0 0 20 20"
                                                        class="Polaris-Icon__Svg"
                                                        focusable="false" aria-hidden="true">
                                                    <path
                                                        d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2"
                                                        fill-rule="evenodd"></path>
                                                    </svg>
                                                </span>
                                            </button>
                                            <button type="button"
                                                    id="btnNext"
                                                    class="Polaris-Pagination__Button"
                                                    aria-label="Next">
                                                <span
                                                    class="Polaris-Icon">
                                                    <svg
                                                        viewBox="0 0 20 20"
                                                        class="Polaris-Icon__Svg"
                                                        focusable="false" aria-hidden="true">
                                                    <path
                                                        d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414"
                                                        fill-rule="evenodd"></path>
                                                    </svg>
                                                </span>
                                            </button>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            <div class="influencer_footer">
                                <div class="Polaris-FooterHelp">
                                    <div class="Polaris-FooterHelp__Content">
                                        <div class="Polaris-FooterHelp__Icon">
                                            <span
                                                class="Polaris-Icon Polaris-Icon--colorTeal Polaris-Icon--isColored Polaris-Icon--hasBackdrop">
                                                <svg
                                                    viewBox="0 0 20 20"
                                                    class="Polaris-Icon__Svg" focusable="false"
                                                    aria-hidden="true">
                                                <circle cx="10" cy="10" r="9"
                                                        fill="currentColor"></circle>
                                                <path
                                                    d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m0-4a1 1 0 1 0 0 2 1 1 0 1 0 0-2m0-10C8.346 4 7 5.346 7 7a1 1 0 1 0 2 0 1.001 1.001 0 1 1 1.591.808C9.58 8.548 9 9.616 9 10.737V11a1 1 0 1 0 2 0v-.263c0-.653.484-1.105.773-1.317A3.013 3.013 0 0 0 13 7c0-1.654-1.346-3-3-3">
                                                </path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="Polaris-FooterHelp__Text">Learn more about
                                            <a class="Polaris-Link"
                                               href="https://help.shopify.com" target="_blank"
                                               data-polaris-unstyled="true">Shopify</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
                                                   function filter_data(action) {
                                                       value = $('#TextField5').val();
                                                       perpage = $('#perpage').val();
                                                       lastItem = $('#lastItem').val();
                                                       $.ajax({
                                                           type: 'get',
                                                           url: "{{route('settings.filter')}}",
                                                           data: {'action': action, 'search': value, 'perpage': perpage, 'lastItem': lastItem},
                                                           success: function (data) {
                                                               if (action === 'search') {
                                                                   $('#currentpage').val('1'); //set current page as initial = 1
                                                                   $('#totalpages').val(JSON.parse(data).totalpages);  //set total pages for search criteria
                                                                   settings = JSON.parse(data).settings;
                                                               } else {
                                                                   settings = JSON.parse(data);
                                                               }
                                                               printData(settings); //print ajax response for pagination
                                                               checkPaginationButtons();
                                                           }
                                                       });
                                                   }

                                                   function printData(settings) {
                                                       $("#tablesettings tr.table-row").remove();
                                                       if (settings.length >= 1) {
                                                       $.each(settings, function (i, item) {

                                                           var route = "{{route('settings.edit','id')}}";
                                                           route = route.replace('id', item.id);
                                                           var tr = '<tr class="Polaris-DataTable__TableRow table-row">';
                                                           tr += '<td class="" style="height: 55px;"><a href="' + route + '">' + item.name + '</a></td>';
                                                           tr += '<td class="" style="height: 55px;">' + item.value + '</td>';
                                                           tr += '</tr>';
                                                           $("#tablesettings tr:last").after(tr);
                                                       });
                                                       }else{
                                                                var tr = '<tr class="Polaris-DataTable__TableRow table-row">';
                                                                tr += '<td colspan="4" class="Polaris-DataTable__Cell vertical-align-center text-center"  style="text-align: center !important;"> No record found.</td>';
                                                                tr += '</tr>';
                                                                $("#tablesettings tr:last").after(tr);
                                                       }
                                                   }

                                                   function checkPaginationButtons() {
                                                       pages = $('#totalpages').val();
                                                       current = $('#currentpage').val();

                                                       $('#btnNext').prop('disabled', false);
                                                       $('#btnPrev').prop('disabled', false);

                                                       if (current === pages) {
                                                           $('#btnNext').prop('disabled', true);
                                                       }
                                                       if (current === '1') {
                                                           $('#btnPrev').prop('disabled', true);
                                                       }
                                                   }

                                                   $(document).ready(function () {   
                                                                                               
                                                   if (typeof $('#texttost').val() !== "undefined" && $('#texttost').val() != ''){
                                                          var msg = $('#texttost').val();
                                                            const toastNotice = actions.Toast.create(crawlapp, {message: msg});
                                                             toastNotice.dispatch(actions.Toast.Action.SHOW);        
                                                        } 
                                                       checkPaginationButtons();
                                                       $('#btnPrev').click(function () {
                                                           perpage = parseInt($('#perpage').val());
                                                           lastItem = $('#lastItem').val();
                                                           $('#lastItem').val(parseInt(lastItem) - perpage);

                                                           currentpage = $('#currentpage').val();
                                                           $('#currentpage').val(parseInt(currentpage) - 1);
                                                           filter_data('paginate');
                                                       });

                                                       $('#btnNext').click(function () {
                                                           perpage = parseInt($('#perpage').val());
                                                           lastItem = $('#lastItem').val();
                                                           $('#lastItem').val(parseInt(lastItem) + perpage);

                                                           currentpage = $('#currentpage').val();
                                                           $('#currentpage').val(parseInt(currentpage) + 1);

                                                           filter_data('paginate');
                                                       });
                                                   });
</script>
@endsection