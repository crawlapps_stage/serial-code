@extends('layouts.master')
@section('content')
    <div class="Polaris-Page">
        <div class="Polaris-Page-Header Polaris-Page-Header--hasNavigation Polaris-Page-Header--hasActionMenu Polaris-Page-Header--mobileView">
            <div class="Polaris-Page-Header__MainContent">
                <div class="Polaris-Page-Header__Title">
                    <div>
                        <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Settings</h1>
                    </div>
                </div>
            </div>
        </div>
        <form id="serial_code_form" method="POST" action="{{ route('settings.store') }}" enctype="multipart/form-data"
              autocomplete="off">
            @csrf
            @if(!empty($settings->id))
                <input type="hidden" name="id" id="settingsid" value="{{ $settings->id }}"/>
            @endif
            <div class="Polaris-Page__Content add-main-row">
                <div class="add-item-main" style="width:100% !important">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div class="active-dates-row">
                                        <div class="w-50">
                                        </div>
                                    </div>
                                    <div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">
                                        <label class="Polaris-Choice" for="Checkbox2">
                                    <span class="Polaris-Choice__Control"><span class="Polaris-Checkbox">
                                        <input id="Checkbox2" name="send_notification" type="checkbox"
                                               class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox"
                                               aria-checked="false" value="1" {{ isset($settings->send_notification)&& $settings->send_notification =='1' ? 'checked' : ''}}><span
                                                    class="Polaris-Checkbox__Backdrop"></span><span
                                                    class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg
                                                            viewBox="0 0 20 20" class="Polaris-Icon__Svg"
                                                            focusable="false" aria-hidden="true">
              <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                                <div class="Polaris-Label"><label id="TextField1Label" for="TextField1"
                                                                                  class="Polaris-Label__Text"></label></div>
            </svg></span></span></span></span><span class="Polaris-Choice__Label">Serial Code Notification Via Mail.</span></label>
                                    </div>

                                    <div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;">
                                        <br>
                                        <label> Copy and Paste the below 2 shortcode in the page template <a href="https://prnt.sc/pgr0gq" target="_blank"> See Example </a></label>
                                        <br>
                                        <code>
                                            <p>
                                                &lt;div id="crawlapps_serialcode_app"&gt;&lt;/div&gt;
                                            </p>
                                            <p>
                                                &lt;input type="hidden" id="domain" value="XXXX.myshopify.com" /&gt;
                                            </p>
                                        </code>
                                    </div>
                                    <div class="Polaris-PageActions" style="border-top-width: 0px; padding-top: 60px;">
                                        <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing">
                                            <div class="Polaris-Stack__Item">
                                                <div class="Polaris-ButtonGroup">
                                                    <div class="Polaris-Stack__Item">
                                                        <button type="submit"
                                                                class="Polaris-Button Polaris-Button--primary"><span
                                                                    class="Polaris-Button__Content"><span
                                                                        class="Polaris-Button__Text">Save</span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-ButtonGroup__Item">
                                                <a href="{{route('serialcode.index')}}" style="color:black;">
                                                    <button type="button" class="Polaris-Button"><span
                                                                class="Polaris-Button__Content"><span
                                                                    class="Polaris-Button__Text">Cancel</span></span>
                                                    </button>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
        @if(session()->get('success'))
            <div class="tost_msg">
                <input type="hidden" name="texttost" id="texttost" value="{{ session()->get('success') }}"/>
            </div>
        @endif
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var settings = $('#settingsid').val();
            if (typeof $('#settingsid').val() == "undefined" || $('#settingsid').val() == '') {
                $("#Checkbox2").prop("checked", true);
            }
            if (typeof $('#texttost').val() !== "undefined" && $('#texttost').val() != '') {
                var msg = $('#texttost').val();
                const toastNotice = actions.Toast.create(crawlapp, {message: msg});
                toastNotice.dispatch(actions.Toast.Action.SHOW);
            }
        });
    </script>
@endsection