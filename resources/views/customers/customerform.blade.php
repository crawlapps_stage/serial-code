<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="icon" href="Favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title> Serialcode form</title>
</head>
<style type="text/css">
    
.crawlapp_form{    
    margin-bottom: 0px !important;
}

.alert{
                position: relative;
                padding: 16px 18px;
                margin-bottom: 15px;
                border: 2px solid transparent;
                border-radius: 8px;
                display: flex;
                align-items: center;
                font-weight: 600;
            }
            .alert img{
                width: 23px;
                margin-right: 15px;
            }
            .alert-danger{
                color: #ea2b1d;
                border-color: #ea2b1d;
            }
            .alert-warning{
                color:#ea7d1d;
                border-color: #ea7d1d;
            }
            .alert-success{
                color:#4ba921;
                border-color: #4ba921;
            }
            .alert-neutral{
                color:#000;
                border-color: #cacaca;
            }
</style>
<body>
    <div class="alert alert-danger" id="danger_massage" style="display: none;">
            <img src="{{ asset('image/cancel.svg') }}" onclick="remove_message()" /> 
            <span id="serialcode_message" style="color:red" > </span>
        </div>
        <div class="alert alert-success" id="successr_massage"style="display: none;">
            <img src="{{ asset('image/tick.svg') }}" onclick="remove_message()">
              <span id="serialcode_suceess_message"> </span>
        </div>  

<main class="my-form" id="mail_div">          
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"></div>
                        <div class="card-body" id="form_div">
                                <div class="form-group row" id="form_serailcode">
                                    <label for="serialcode" class="col-md-4 col-form-label text-md-right">Serial Code</label>
                                    <div class="col-md-6">
                                        <input type="text" id="serialcode" class="form-control crawlapp_form" name="serialcode"  value="">
                                        <p id="serialcode_err" style="color:red"> </p>
                                    </div>                                   
                                </div>
                                 <div class="form-group row" id="form_firstname" style="display: none">
                                    <label for="firstname" class="col-md-4 col-form-label text-md-right">First name</label>
                                    <div class="col-md-6">
                                        <input type="text" id="firstname" class="form-control crawlapp_form" name="firstname" value="">
                                        <p id="firstname_err" class="error_message"  style="color:red"> </p>
                                    </div>
                                </div>
                                <div class="form-group row" id="form_lastname" style="display: none">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">Last name</label>
                                    <div class="col-md-6">
                                        <input type="text" id="lastname" class="form-control crawlapp_form" name="lastname"  value="">
                                         <p id="lastname_err" class="error_message" style="color:red"> </p>
                                    </div>
                                </div>

                                <div class="form-group row" id="form_email" style="display: none">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail</label>
                                    <div class="col-md-6">
                                        <input type="text" id="user_mail" class="form-control crawlapp_form" name="email" onkeyup="ValidateEmail()"  value="">
                                        <p id="email_err" class="error_message" style="color:red"> </p>
                                    </div>
                                </div>

                                <div class="form-group row" id="form_product" style="display: none"> 
                                    <label for="product_id" class="col-md-4 col-form-label text-md-right" >Product</label>
                                    <div class="col-md-6">
                                     <select class="form-control crawlapp_form" name="product_id" id="product_id">
                                                <option value="">Select product</option>
                                            </select>                                     
                                    </div>
                                </div>                              
                                      <div class="col-md-6 offset-md-4" id="form_submit">
                                        <button onclick="submitform()" id="submit_button"  class="btn btn-primary">
                                        Submit
                                        </button> <span id="message" style="color:red"> </span> 
                                    </div>
                                </div>
                                     <p id="domain_err" style="color:red; text-align:center; "> </p>
                                </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

</main>

</body>
</html>
