@extends('layouts.master')
@section('content')

<div class="Polaris-Page">
            <div class="Polaris-Page-Header Polaris-Page-Header--hasNavigation Polaris-Page-Header--hasActionMenu Polaris-Page-Header--mobileView">
                <div class="Polaris-Page-Header__MainContent">
                    <div class="Polaris-Page-Header__Title">
                        <div>
                        <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Email Template</h1>
                        </div>
                    </div>
                </div>
            </div>
            <form id="email_template" method="POST" action="{{ route('emailtemplate.store') }}" enctype="multipart/form-data" autocomplete="off">
               @csrf
            <div class="Polaris-Page__Content add-main-row">
                <div class="add-item-main">
                   <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 class="Polaris-Heading">Subject</h2>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div>
                                        <div class="Polaris-Filters">
                                         <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                                <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                        <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                            <div class="Polaris-Labelled--hidden">
                                                                <div class="Polaris-TextField">
                                                                    <input id="TextField1"  class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="{{ old('subject', isset($emailemplate->subject) ? $emailemplate->subject : '') }}" name="subject" placeholder="Enter Subject">
                                                                    <div class="Polaris-TextField__Backdrop"></div>

                                                                </div>

                                                        @if($errors->has('subject'))
                                                            <div class="Polaris-Labelled__Error">
                                                                <div id="TextField1Error" class="Polaris-InlineError">
                                                                    <div class="Polaris-InlineError__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                            <path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-8h2V6H9v4zm0 4h2v-2H9v2z" fill-rule="evenodd"></path>
                                                                            </svg></span></div>
                                                                    {{ $errors->first('subject') }}
                                                                </div>
                                                            </div>
                                                            @endif                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="Polaris-Card__Header">
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                <h2 class="Polaris-Heading">Content</h2>
                                </div>

                            </div>
                        </div>
                        <div class="Polaris-Card__Section pt-1">
                            <div class="Polaris-FormLayout">
                                <div class="Polaris-FormLayout__Item">
                                    <div class="label-extra-text">
                                        <div class="Polaris-TextField">
                                            <textarea name="content" rows="10" class="Polaris-TextField__Input"> {{ old('content', isset($emailemplate->content) ? $emailemplate->content : '') }} </textarea>
                                        <div class="Polaris-TextField__Backdrop"></div>

                                        </div>
                                        @if($errors->has('content'))
                                                            <div class="Polaris-Labelled__Error">
                                                                <div id="TextField1Error" class="Polaris-InlineError">
                                                                    <div class="Polaris-InlineError__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                            <path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-8h2V6H9v4zm0 4h2v-2H9v2z" fill-rule="evenodd"></path>
                                                                            </svg></span></div>
                                                                    {{ $errors->first('content') }}
                                                                </div>
                                                            </div>
                                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="Polaris-PageActions">
                <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing">
                    <div class="Polaris-Stack__Item">
                    <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button></div>
                    </div>
                    </div>
                     <div class="Polaris-Stack__Item"><button type="submit" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save</span></span></button></div>
                </div>
            </div>
          </form>
           @if(session()->get('success'))
                                            <div class="tost_msg">
                                                <input type="hidden" name="texttost" id="texttost" value="{{ session()->get('success') }}" />
                                            </div>
                                      @endif
        </div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var settings = $('#settingsid').val();
    if(settings == ''){
    $( "#Checkbox2" ).prop( "checked", true );
    }
    if (typeof $('#texttost').val() !== "undefined" && $('#texttost').val() != ''){
          var msg = $('#texttost').val();
          const toastNotice = actions.Toast.create(crawlapp, {message: msg});
          toastNotice.dispatch(actions.Toast.Action.SHOW);
        }
    });
</script>
@endsection