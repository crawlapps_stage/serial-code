@extends('layouts.master')
@section('content')
       <div class="Polaris-Page">
            <div class="Polaris-Page-Header Polaris-Page-Header--hasNavigation Polaris-Page-Header--hasActionMenu Polaris-Page-Header--mobileView">
            <div class="Polaris-Page-Header__MainContent">
                <div class="Polaris-Page-Header__TitleActionMenuWrapper">
                <div class="Polaris-Page-Header__Title">
                    <div>
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Serial numbers</h1>
                    <button type="button" class="Polaris-ActionList__Item">
                        <div class="Polaris-ActionList__Content">
                            <div class="Polaris-ActionList__Image"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                <path d="M13.707 6.707a.997.997 0 0 1-1.414 0L11 5.414V13a1 1 0 1 1-2 0V5.414L7.707 6.707a.999.999 0 1 1-1.414-1.414l3-3a.999.999 0 0 1 1.414 0l3 3a.999.999 0 0 1 0 1.414zM17 18H3a1 1 0 1 1 0-2h14a1 1 0 1 1 0 2z"></path>
                                </svg></span></div>
                            <div class="Polaris-ActionList__Text">Export file</div>
                        </div>
                    </button>
                    </div>
                    
                </div>
                <div class="Polaris-Page-Header__ActionMenuWrapper">
                    <button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">create Serial numbers</span></span></button>
                </div>
                </div>
            </div>
            </div>
            <div class="Polaris-Page__Content">
                <div class="Polaris-Card">
                    <div>
                        <ul role="tablist" class="Polaris-Tabs">
                            <li role="presentation" class="Polaris-Tabs__TabContainer"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">All</span></button></li>
                            <li role="presentation" class="Polaris-Tabs__TabContainer"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">Registered</span></button></li>
                            <li role="presentation" class="Polaris-Tabs__TabContainer"><button id="repeat-customers" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="repeat-customers-content"><span class="Polaris-Tabs__Title">Unregistered</span></button></li>
                        </ul>
                        <div class="Polaris-Tabs__Panel" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
                            <div class="Polaris-Card__Section">
                                <div class="">
                                    <div class="Polaris-Filters">
                                        <div class="Polaris-Filters-ConnectedFilterControl__Wrapper">
                                            <div class="Polaris-Filters-ConnectedFilterControl Polaris-Filters-ConnectedFilterControl--right">
                                                <div class="Polaris-Filters-ConnectedFilterControl__RightContainer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div>
                                                            <button type="button" class="Polaris-Button left-button" tabindex="0" aria-controls="Popover1" aria-owns="Popover1" aria-haspopup="true" aria-expanded="undefined">
                                                                <span class="Polaris-Button__Content">
                                                                    <span class="Polaris-Button__Text">Fillter</span>
                                                                    <span class="Polaris-Button__Icon">
                                                                        <span class="Polaris-Icon">
                                                                            <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Filters-ConnectedFilterControl__CenterContainer">
                                                    <div class="Polaris-Filters-ConnectedFilterControl__Item">
                                                        <div class="Polaris-Labelled--hidden">
                                                            <div class="Polaris-TextField">
                                                                <div class="Polaris-TextField__Prefix" id="TextField1Prefix">
                                                                    <span class="Polaris-Filters__SearchIcon">
                                                                        <span class="Polaris-Icon">
                                                                            <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <input id="TextField1" placeholder="Filter customers" class="Polaris-TextField__Input Polaris-TextField__Input--hasClearButton" aria-labelledby="TextField1Label TextField1Prefix" aria-invalid="false" value="">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="serial-list-section">
                                    <div class="Polaris-Page-Header--separator serial-list-header">
                                        <label class="Polaris-Choice" for="Checkbox1">
                                            <span class="Polaris-Choice__Control">
                                                <span class="Polaris-Checkbox">
                                                    <input id="Checkbox1" type="checkbox" class="Polaris-Checkbox__Input">
                                                    <span class="Polaris-Checkbox__Backdrop"></span>
                                                    <span class="Polaris-Checkbox__Icon">
                                                        <span class="Polaris-Icon">
                                                            <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="Polaris-Choice__Label">Showing 1 Serial Code</span>
                                        </label>
                                        <div class="select-label">
                                            <div class="Polaris-Labelled__LabelWrapper">
                                                <div class="Polaris-Label"><label id="Select9Label" for="Select9" class="Polaris-Label__Text">Date range</label></div>
                                            </div>
                                            <div class="Polaris-Select">
                                                <select class="Polaris-Select__Input">
                                                    <option>Last create</option>
                                                    <option>Last create</option>
                                                    <option>Last create</option>
                                                </select>
                                                <div class="Polaris-Select__Content" aria-hidden="true"><span class="Polaris-Select__SelectedOption">Last create</span><span class="Polaris-Select__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                        <path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path>
                                                    </svg></span></span></div>
                                                <div class="Polaris-Select__Backdrop"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="serial-list">
                                        <li>
                                            <div>
                                            <label class="Polaris-Choice" for="Checkbox2"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="Checkbox2" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value=""><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                              </svg></span></span></span></span></label>
                                            </div>
                                            <div class="serial-list-text">
                                                <h3 class="Polaris-Subheading">W584-WDG64-J6RT</h3>
                                                <p>Shopify POS is the easiest way to sell your products in person. Available for iPad, iPhone, and Android.</p>
                                                <span class="Polaris-Badge Polaris-Badge--statusSuccess">Active</span>
                                                <div class="Polaris-TextStyle--variationSubdued date-serial">From May 22</div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

