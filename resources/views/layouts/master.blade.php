<!DOCTYPE html>
<html lang="en">
    @include('layouts.header')
    <body> 
       <div class="Polaris-Card">
        <div>
            <ul role="tablist" class="Polaris-Tabs">
                <li  role="presentation" class="Polaris-Tabs__TabContainer">
                  <a href="{{route('serialcode.index')}}" style="color:black;">
                     <button tag="button" class="Polaris-Tabs__Tab ">
                        <span class="Polaris-Tabs__Title">Serialcode</span>
                    </button>
                </a>
                </li>

                <li  role="presentation" class="Polaris-Tabs__TabContainer">
                	<a href="{{route('customers.index')}}" style="color:black;">
                    <button tag="button" class="Polaris-Tabs__Tab ">
                        <span class="Polaris-Tabs__Title">Customer</span>
                    </button>
                </a>
                </li>
                 <li  role="presentation" class="Polaris-Tabs__TabContainer">
                 	<a href="{{route('settings.create')}}" style="color:black;">
                    <button tag="button" class="Polaris-Tabs__Tab ">
                        <span class="Polaris-Tabs__Title">Settings</span>
                    </button>
                </a>
                </li>
               <li  role="presentation" class="Polaris-Tabs__TabContainer">
                    <a href="{{route('emailtemplate.create')}}" style="color:black;">
                    <button tag="button" class="Polaris-Tabs__Tab ">
                        <span class="Polaris-Tabs__Title">Email Template</span>
                    </button>
                </a>
                </li>
            </ul>
        </div>
    </div>       
     @yield('content')
    </body>
</html>