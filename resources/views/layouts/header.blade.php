<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    
      <link rel="stylesheet" href="https://sdks.shopifycdn.com/polaris/3.20.0/polaris.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.1.2/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/my.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/polaris.css') }}" />
    @if(config('shopify-app.appbridge_enabled'))
            <script src="https://unpkg.com/@shopify/app-bridge"></script>
            <script>
                var AppBridge = window['app-bridge'];
                var actions = AppBridge.actions;

                var createApp = AppBridge.default;
                var crawlapp = createApp({
                    apiKey: '{{ config('shopify-app.api_key') }}',
                    shopOrigin: '{{ ShopifyApp::shop()->shopify_domain }}',
                    forceRedirect: true,
                });
            </script>

        @endif
        @yield('styles')
    </head>